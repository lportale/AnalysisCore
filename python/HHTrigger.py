import os
from array import array

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from analysis_tools.utils import import_root
from Tools.Tools.jet_utils import JetPair
from Tools.Tools.utils_trigger import get_trigger_requests, get_trigger_list
from Base.Modules.baseModules import JetLepMetModule, JetLepMetSyst

ROOT = import_root()

class HHPreTriggerRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.runPeriod = kwargs.pop("runPeriod")

        trigger_list = (get_trigger_list(self.year, self.runPeriod, self.isMC, "HLT_Mu") +
                        get_trigger_list(self.year, self.runPeriod, self.isMC, "HLT_Ele") +
                        get_trigger_list(self.year, self.runPeriod, self.isMC, "HLT_MuTau") +
                        get_trigger_list(self.year, self.runPeriod, self.isMC, "HLT_EleTau") +
                        get_trigger_list(self.year, self.runPeriod, self.isMC, "HLT_DiTau") +
                        get_trigger_list(self.year, self.runPeriod, self.isMC, "HLT_DiTauJet") +
                        get_trigger_list(self.year, self.runPeriod, self.isMC, "HLT_QuadJet") +
                        get_trigger_list(self.year, self.runPeriod, self.isMC, "HLT_VBF") )
        if None in trigger_list:
            trigger_list.remove(None)
        self.trigger_preselection = "(" + " || ".join(trigger_list) + ")"

    def run(self, df):
        branches = []
        df = df.Filter(self.trigger_preselection)
        return df, branches

def HHPreTriggerRDF(**kwargs):
    """
    Preselects the events satisfying the OR of all triggers that are going to be used.
    This has no impact on the analysis workflow, but reduces substantially the number of
    events to process, especially in background MC samples.

    :param isMC: whether the dataset os DATA or MC
    :type isMC: bool

    :param year: data taking year
    :type year: int

    YAML sintax:

    .. code-block:: yaml

        codename:
            name: HHPreTriggerRDF
            path: Tools.Tools.HHTrigger
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
    """
    return lambda: HHPreTriggerRDFProducer(**kwargs)


class HHTriggerRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHTriggerRDFProducer, self).__init__(self, *args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        self.runPeriod = kwargs.pop("runPeriod")
        self.dataset = kwargs.pop("dataset")
        self.NanoAODv = kwargs.pop("NanoAODv")
        self.pass_filter = kwargs.pop("pass_filter", True)

        if not os.getenv("_HHTrigger"):
            os.environ["_HHTrigger"] = "HHTrigger"

            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")

            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))

            ROOT.gROOT.ProcessLine(".L {}/interface/HHTriggerInterface.h".format(base))

            ROOT.gInterpreter.Declare("""
                auto HHTrigger = HHTriggerInterface();
            """)
            
            ROOT.gInterpreter.Declare("""
                std::vector<trig_req> get_triggers(
                        int run, std::vector<std::vector<double>> runrange,
                        std::vector<trig_req> requests) {
                    std::vector<trig_req> out_trig_reqs;
                    for (size_t i = 0; i < runrange.size(); i++) {
                        if (run < runrange[i][0] || run > runrange[i][1]) continue;
                        else out_trig_reqs.push_back(requests[i]);
                    }
                    return out_trig_reqs;
                }
            """)

    def run(self, df):
        variables = [
            "pass_triggers", "isQuadJetTrigger", "isTauTauJetTrigger", "isVBFtrigger", 
            "QuadJetMatchedToHLT"
        ]

        df = df.Define("hlt_mu_leptonsRequest", "get_triggers(run, %s, %s)" %
                       (get_trigger_requests(self.year, self.runPeriod, self.NanoAODv, self.isMC, "HLT_Mu", "leptons")))
        
        df = df.Define("hlt_ele_leptonsRequest", "get_triggers(run, %s, %s)" %
                       (get_trigger_requests(self.year, self.runPeriod, self.NanoAODv, self.isMC, "HLT_Ele", "leptons")))
        
        df = df.Define("hlt_mutau_leptonsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.runPeriod, self.NanoAODv, self.isMC, "HLT_MuTau", "leptons")))
        
        df = df.Define("hlt_eletau_leptonsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.runPeriod, self.NanoAODv, self.isMC, "HLT_EleTau", "leptons")))
        
        df = df.Define("hlt_ditau_leptonsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.runPeriod, self.NanoAODv, self.isMC, "HLT_DiTau", "leptons")))
        
        df = df.Define("hlt_ditaujet_lepjetsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.runPeriod, self.NanoAODv, self.isMC, "HLT_DiTauJet", "lepjets")))
        
        df = df.Define("hlt_quadjet_jetsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.runPeriod, self.NanoAODv,  self.isMC, "HLT_QuadJet", "jets")))
        
        df = df.Define("hlt_vbf_leptonsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.runPeriod, self.NanoAODv, self.isMC, "HLT_VBF", "leptons")))
        
        df = df.Define("hh_trigger_results", "HHTrigger.get_trigger_output("
            "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
            "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
            "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
            "Jet_pt{3}, Jet_eta, Jet_phi, Jet_mass{3}, "
            "pairType, dau1_index, dau2_index, "
            "hasResolvedAK4, hasBoostedAK8, hasVBFAK4, "
            "bjet1_JetIdx, bjet2_JetIdx, VBFjet1_JetIdx, VBFjet2_JetIdx, "
            "TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, "
            "hlt_mu_leptonsRequest, hlt_ele_leptonsRequest, "
            "hlt_mutau_leptonsRequest, hlt_eletau_leptonsRequest, "
            "hlt_ditau_leptonsRequest, hlt_ditaujet_lepjetsRequest, "
            "hlt_quadjet_jetsRequest, hlt_vbf_leptonsRequest, \"{4}\", {5} "
            ")".format(self.muon_syst, self.electron_syst, 
            self.tau_syst, self.jet_syst, self.dataset,
            "true" if self.isMC else "false"))
        branches = []
        for var in variables:
            df = df.Define(var, "hh_trigger_results.%s" % var)
            branches.append(var)

        if self.pass_filter:
            df = df.Filter("pass_triggers")

        return df, branches

def HHTriggerRDF(**kwargs):
    """
    Applies the complete trigger selection for all triggers and objects.
    Returns flags for if and which triggers were passed.

    Returns the daughter indexes, angular and isolation variables, whether event is with opposite charge leptons,
    if and which jet triggers were used, HHbtag output of the event, the indexes from the 2 bjets and 2 vbfjets (if existing),
    the indexes of the additional central and forward jets (if existing).

    Lepton and jet systematics (used for pt and mass variables) can be modified using the parameters
    from :ref:`BaseModules_JetLepMetSyst`.
    
    :param isMC: whether the dataset os DATA or MC
    :type isMC: bool

    :param year: data taking year
    :type year: int

    :param NanoAODv: version of the NanoAOD production
    :type NanoAODv: str

    :param pass_filter: whether to filter out output events if they don't pass the triggers
    :type pass_filter: bool

    YAML sintax:

    .. code-block:: yaml

        codename:
            name: HHTriggerRDF
            path: Tools.Tools.HHTrigger
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                NanoAODv: self.dataset.tags[0]
                pass_filter: True
    """
    return lambda: HHTriggerRDFProducer(**kwargs)
