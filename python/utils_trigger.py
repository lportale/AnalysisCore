import re
import os
import json
import envyaml
from math import sqrt
from Tools.Tools.utils_lepton import deltaR
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection

def load_json(path):
        with open(path) as f:
            d = json.load(f)
        return d


def periodTrigWrapper(triggerSubCfg, runPeriod):
    # This function takes care of the exception due to changes in triggers for different periods
    # of data taking. As this happens rarely, to avoid codes duplicate in the HHTriggers*.yaml, the
    # check for a possible change is always performed and exceptions caught.
    try:
        return triggerSubCfg[runPeriod]
    except TypeError:
        return triggerSubCfg


def get_trigger_requests(year, runPeriod, NanoAODv, isMC, HLTcategory, objects):
    if year == 2016 or year == 2017 or year == 2018:
        path = os.path.expandvars("$CMSSW_BASE/src/Tools/Tools/data/HHtriggers_Run2.yaml")

    elif year == 2022 or year == 2023:
        path = os.path.expandvars("$CMSSW_BASE/src/Tools/Tools/data/HHtriggers_Run3.yaml")

    else:
        raise ValueError("Required year not implemented yet")
    
    triggerCfg = envyaml.EnvYAML(path)

    if isMC: DATAorMC = "MC"
    else:    DATAorMC = "DATA"

    triggerReqStr = 'trig_req{ %s, "%s", {%s}, {%s}, {%s}, {%s}, {%s} }'
    triggerRRStr  = '{%d,%d}'

    triggerRequests = []
    triggerRequestsRR = []
    for HLTpath in periodTrigWrapper(triggerCfg["YEARS"][year][DATAorMC][HLTcategory], runPeriod):
        if HLTpath == None: continue
        pathCfg = triggerCfg["HLTpaths"][HLTpath][objects]

        dataset_assignement = triggerCfg["HLTpaths"][HLTpath]["dataset"]
        
        pts = [str(pt) for pt in pathCfg["pt"]]
        etas = [str(eta) for eta in pathCfg["eta"]]
        ids = [str(ID) for ID in pathCfg["id"]]

        fbits = []
        for idx, obj in enumerate(pathCfg["FilterBits"]):
            filterBits = []
            for filterBit in pathCfg["FilterBits"][obj]:
                if NanoAODv != "None":
                    filterBits.append(str(triggerCfg["FilterBits"][NanoAODv][obj[:-1]][filterBit]))
                else:
                    filterBits.append(str(triggerCfg["FilterBits"][obj[:-1]][filterBit]))

            fbits.append("{"+",".join(filterBits)+"}")
        
        additional_fbits_for_mand_objs = []
        if "AdditionalFilterBitsForMandatoryObjs" in pathCfg:
            for idx, obj in enumerate(pathCfg["AdditionalFilterBitsForMandatoryObjs"]):
                filterBits  = []
                for filterBit in pathCfg["AdditionalFilterBitsForMandatoryObjs"][obj]:
                    if NanoAODv != "None":
                        filterBits.append(str(triggerCfg["FilterBits"][NanoAODv][obj[:-1]][filterBit]))
                    else:
                        filterBits.append(str(triggerCfg["FilterBits"][obj[:-1]][filterBit]))
                additional_fbits_for_mand_objs.append("{"+",".join(filterBits)+"}")

        triggerReq = triggerReqStr % (HLTpath,
                                      dataset_assignement,
                                      ",".join(pts),
                                      ",".join(etas),
                                      ",".join(ids),
                                      ",".join(fbits),
                                      ",".join(additional_fbits_for_mand_objs))

        try:
            triggerReqRR = pathCfg["runrange"]
            triggerRR = triggerRRStr % (triggerReqRR[0], triggerReqRR[1])
        except KeyError:
            triggerRR = triggerRRStr % (-1, 1E9)

        triggerRequests.append(triggerReq)
        triggerRequestsRR.append(triggerRR)

    triggerRequestsOut = "{" + ", ".join(triggerRequests) + "}"
    triggerRequestsRROut = "{" + ", ".join(triggerRequestsRR) + "}"

    return triggerRequestsRROut, triggerRequestsOut


def get_trigger_list(year, runPeriod, isMC, HLTcategory):
    if year == 2016 or year == 2017 or year == 2018:
        path = os.path.expandvars("$CMSSW_BASE/src/Tools/Tools/data/HHtriggers_Run2.yaml")

    elif year == 2022 or year == 2023:
        path = os.path.expandvars("$CMSSW_BASE/src/Tools/Tools/data/HHtriggers_Run3.yaml")

    else:
        raise ValueError("Required year not implemented yet")

    triggerCfg = envyaml.EnvYAML(path)
    triggerList = []
    for DATAorMC in ["DATA", "MC"]:
        for HLTpath in periodTrigWrapper(triggerCfg["YEARS"][year][DATAorMC][HLTcategory], runPeriod):
            triggerList.append(HLTpath)

    # remove path duplicates
    triggerList = list(dict.fromkeys(triggerList))

    return triggerList


def getSingleCrossThresholds(year, runPeriod):
    hltCfg  = envyaml.EnvYAML('%s/src/Tools/Tools/data/HHtriggers_Run3.yaml' % 
                                    os.environ['CMSSW_BASE'])

    SingleMuPath = "" ; isomuTrg_offMuThr = 0
    CrossMuTauPath = "" ; mutauTrg_offMuThr = 0; mutauTrg_offTauThr = 0
    SingleElePath = "" ; eleTrg_offEleThr = 0
    CrossEleTauPath = "" ; etauTrg_offEleThr = 0; etauTrg_offTauThr = 0
    DiTauPath = "" ; ditauTrg_offTauThr = 0
    DiTauJetPath = "" ; ditaujetTrg_offTauThr = 0; ditaujetTrg_offJetThr = 0

    # check in config what are the lower seeds
    for path in periodTrigWrapper(hltCfg["YEARS"][year]["DATA"]["HLT_Mu"], runPeriod):
        if not path: continue # safety against None
        if "IsoMu" in path:
            prvThr = 9999
            crrThr = int(path.split("IsoMu")[1].split("_")[0])
            if crrThr < prvThr:
                SingleMuPath = path
    for path in periodTrigWrapper(hltCfg["YEARS"][year]["DATA"]["HLT_MuTau"], runPeriod):
        if not path: continue # safety against None
        if "IsoMu" in path and "TauHPS" in path:
            prvThr = 9999
            crrThr = int(path.split("IsoMu")[1].split("_")[0])
            if crrThr < prvThr:
                CrossMuTauPath = path
    for path in periodTrigWrapper(hltCfg["YEARS"][year]["DATA"]["HLT_Ele"], runPeriod):
        if not path: continue # safety against None
        if "Ele" in path:
            prvThr = 9999
            crrThr = int(path.split("Ele")[1].split("_")[0])
            if crrThr < prvThr:
                SingleElePath = path
    for path in periodTrigWrapper(hltCfg["YEARS"][year]["DATA"]["HLT_EleTau"], runPeriod):
        if not path: continue # safety against None
        if "Ele" in path and "TauHPS" in path:
            prvThr = 9999
            crrThr = int(path.split("Ele")[1].split("_")[0])
            if crrThr < prvThr:
                CrossEleTauPath = path
    for path in periodTrigWrapper(hltCfg["YEARS"][year]["DATA"]["HLT_DiTau"], runPeriod):
        if not path: continue # safety against None
        if "DoubleMediumDeepTau" in path and not "PFJet" in path:
            prvThr = 9999
            crrThr = int(path.split("TauHPS")[1].split("_")[0])
            if crrThr < prvThr:
                DiTauPath = path
    for path in periodTrigWrapper(hltCfg["YEARS"][year]["DATA"]["HLT_DiTauJet"], runPeriod):
        if not path: continue # safety against None
        if "DoubleMediumDeepTau" in path and "PFJet" in path:
            prvThr = 9999
            crrThr = int(path.split("TauHPS")[1].split("_")[0])
            if crrThr < prvThr:
                DiTauJetPath = path

    if SingleMuPath    != "": isomuTrg_offMuThr     = hltCfg["HLTpaths"][SingleMuPath]["leptons"]["pt"][0]
    if CrossMuTauPath  != "":
        mutauTrg_offMuThr = hltCfg["HLTpaths"][CrossMuTauPath]["leptons"]["pt"][0]
        mutauTrg_offTauThr = hltCfg["HLTpaths"][CrossMuTauPath]["leptons"]["pt"][1]
    if SingleElePath   != "": eleTrg_offEleThr      = hltCfg["HLTpaths"][SingleElePath]["leptons"]["pt"][0]
    if CrossEleTauPath != "":
        etauTrg_offEleThr = hltCfg["HLTpaths"][CrossEleTauPath]["leptons"]["pt"][0]
        etauTrg_offTauThr = hltCfg["HLTpaths"][CrossEleTauPath]["leptons"]["pt"][1]
    if DiTauPath       != "": ditauTrg_offTauThr    = hltCfg["HLTpaths"][DiTauPath]["leptons"]["pt"][0]
    if DiTauJetPath    != "":
        ditaujetTrg_offTauThr = hltCfg["HLTpaths"][DiTauJetPath]["leptons"]["pt"][0]
        ditaujetTrg_offJetThr = hltCfg["HLTpaths"][DiTauJetPath]["jets"]["pt"][0]

    return (isomuTrg_offMuThr, mutauTrg_offMuThr, mutauTrg_offTauThr, eleTrg_offEleThr,
            etauTrg_offEleThr, etauTrg_offTauThr, ditauTrg_offTauThr, ditaujetTrg_offTauThr,
            ditaujetTrg_offJetThr)
