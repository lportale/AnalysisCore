#include "Tools/Tools/interface/HHTriggerInterface.h"

// Constructors

HHTriggerInterface::HHTriggerInterface () {};

// destructor
HHTriggerInterface::~HHTriggerInterface() {}

trigger_output HHTriggerInterface::get_trigger_output(
    fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
    fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
    fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    int pairType, int dau1_index, int dau2_index,
    bool hasResolvedAK4, bool hasBoostedAK8, bool hasVBFAK4,
    int bjet1_index, int bjet2_index, int vbfjet1_index, int vbfjet2_index,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<trig_req> hlt_mu_triggers, std::vector<trig_req> hlt_ele_triggers,
    std::vector<trig_req> hlt_mutau_triggers, std::vector<trig_req> hlt_eletau_triggers,
    std::vector<trig_req> hlt_ditau_triggers, std::vector<trig_req> hlt_ditaujet_triggers,
    std::vector<trig_req> hlt_quadjet_triggers, std::vector<trig_req> hlt_vbf_triggers,
    std::string dataset, bool isMC)
{   
    trigger_output trig_out {false, false, false, false, -1};
    if (pairType == -1) 
        return trig_out;

    // MuTau
    if (pairType == 0) {
        auto leps = GetLepsTLV(dau1_index, dau2_index,
            Muon_pt, Muon_eta, Muon_phi, Muon_mass,
            Tau_pt, Tau_eta, Tau_phi, Tau_mass);
        
        std::vector<TLorentzVector> lepton_objs = {leps.first, leps.second};

        if (// check SingleMu triggers
            pass_trigger({lepton_objs[0]},
                         hlt_mu_triggers, dataset, isMC,
                         TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi)
            // check CrossMuTau triggers
            || pass_trigger(lepton_objs,
                            hlt_mutau_triggers, dataset, isMC,
                            TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi)) {
            trig_out.pass_triggers = true;
        }
    }
    
    // ETau
    else if (pairType == 1) {
        auto leps = GetLepsTLV(dau1_index, dau2_index,
            Electron_pt, Electron_eta, Electron_phi, Electron_mass,
            Tau_pt, Tau_eta, Tau_phi, Tau_mass);
        
        std::vector<TLorentzVector> lepton_objs = {leps.first, leps.second};

        if (// check SingleEle triggers
            pass_trigger({lepton_objs[0]},
                         hlt_ele_triggers, dataset, isMC,
                         TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi)
            // check CrossEleTau triggers
            || pass_trigger(lepton_objs,
                            hlt_eletau_triggers, dataset, isMC,
                            TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi)) {
            
            trig_out.pass_triggers = true;
        }
    }

    // TauTau
    else if (pairType == 2) {
        auto leps = GetLepsTLV(dau1_index, dau2_index,
            Tau_pt, Tau_eta, Tau_phi, Tau_mass,
            Tau_pt, Tau_eta, Tau_phi, Tau_mass);
         
        std::vector<TLorentzVector> lepton_objs = {leps.first, leps.second};

        if (// check DiTau triggers
            pass_trigger(lepton_objs,
                         hlt_ditau_triggers, dataset, isMC,
                         TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi) )
        {
            trig_out.pass_triggers = true;
        }
        
        else if (hasResolvedAK4) 
        {
            auto bjets = GetJetsTLV(bjet1_index, bjet2_index,
                            Jet_pt, Jet_eta, Jet_phi, Jet_mass);

            std::vector<TLorentzVector> bjet_objs = {bjets.first, bjets.second};

            if (// check DiTau+Jet trigger with first bjet
                pass_trigger({leps.first, leps.second, bjets.first},
                             hlt_ditaujet_triggers, dataset, isMC,
                             TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi) )
            {
                trig_out.pass_triggers = true;
                trig_out.isTauTauJetTrigger = true;
            }

            if (hasVBFAK4)
            {
                if (// check VBF trigger
                    pass_trigger(lepton_objs,
                                 hlt_vbf_triggers, dataset, isMC,
                                 TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi) )
                {
                    trig_out.pass_triggers = true;
                    trig_out.isVBFtrigger = true;
                }
            }

            // check QuadJet triggers
            bool passQuadJet = pass_trigger({leps.first, leps.second,
                                    bjets.first, bjets.second}, {15, 15, 1, 1},
                                    hlt_quadjet_triggers, dataset, isMC,
                                    TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi);

            // check other triggers to orthogonalise 
            // with events already passing in the Tau dataset
            bool passDiTau_ORTHO = pass_trigger(lepton_objs,
                                        hlt_ditau_triggers, "Tau", isMC,
                                        TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi);

            bool passDiTauJet_ORTHO = ( pass_trigger({leps.first, leps.second, bjets.first},
                                            hlt_ditaujet_triggers, "Tau", isMC,
                                            TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi)
                                      || pass_trigger({leps.first, leps.second, bjets.second},
                                            hlt_ditaujet_triggers, "Tau", isMC,
                                            TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi) );

            bool passVBF_ORTHO = pass_trigger(lepton_objs,
                                    hlt_vbf_triggers, "Tau", isMC,
                                    TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi);

            if (passQuadJet && !(passDiTau_ORTHO || passDiTauJet_ORTHO || passVBF_ORTHO))
            {
                trig_out.pass_triggers = true;
                trig_out.isQuadJetTrigger = true;
            }
        }
    }

    // MuMu
    else if (pairType == 3) {
        auto leps = GetLepsTLV(dau1_index, dau2_index,
            Muon_pt, Muon_eta, Muon_phi, Muon_mass,
            Muon_pt, Muon_eta, Muon_phi, Muon_mass);

        auto lep4trg = TLorentzVector();
        if (leps.first.Pt() > leps.second.Pt())
            lep4trg = leps.first;
        else
            lep4trg = leps.second;

        if (// check Mu triggers
            pass_trigger({lep4trg},
                         hlt_mu_triggers, dataset, isMC,
                         TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi))
        {
            trig_out.pass_triggers = true;
        }
    }

    // EE
    else if (pairType == 4) {
        auto leps = GetLepsTLV(dau1_index, dau2_index,
            Electron_pt, Electron_eta, Electron_phi, Electron_mass,
            Electron_pt, Electron_eta, Electron_phi, Electron_mass);

        auto lep4trg = TLorentzVector();
        if (leps.first.Pt() > leps.second.Pt())
            lep4trg = leps.first;
        else
            lep4trg = leps.second;

        if (// check Ele triggers
            pass_trigger({lep4trg},
                         hlt_ele_triggers, dataset, isMC,
                         TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi))
        {
            trig_out.pass_triggers = true;
        }
    }

    // EMu
    else if (pairType == 5) {
        auto leps = GetLepsTLV(dau1_index, dau2_index,
            Muon_pt, Muon_eta, Muon_phi, Muon_mass,
            Electron_pt, Electron_eta, Electron_phi, Electron_mass);

        // check SingleMu triggers on the muon
        bool passSingleMu = pass_trigger({leps.first},
                                hlt_mu_triggers, dataset, isMC,
                                TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi);

        // check SingleEle triggers on electron
        bool passSingleEle = pass_trigger({leps.second},
                                hlt_ele_triggers, dataset, isMC,
                                TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi);

        // check other triggers to orthogonalise 
        // with events already passing in the Muon dataset
        bool passSingleMu_ORTHO = pass_trigger({leps.first},
                                    hlt_mu_triggers, "Muon", isMC,
                                    TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi);

        if (passSingleMu || (passSingleEle && !passSingleMu_ORTHO))
            trig_out.pass_triggers = true;
    }

    return trig_out;
}