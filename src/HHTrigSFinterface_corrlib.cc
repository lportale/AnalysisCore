#include "Tools/Tools/interface/HHTrigSFinterface_corrlib.h"

HHTrigSFinterface_corrlib::HHTrigSFinterface_corrlib(
    float year,
    float isomuTrg_offMuThr, float mutauTrg_offMuThr, float mutauTrg_offTauThr, 
    float eleTrg_offEleThr, float etauTrg_offEleThr, float etauTrg_offTauThr, 
    float ditauTrg_offTauThr, float ditaujetTrg_offTauThr, float ditaujetTrg_offJetThr,
    std::string eTrgSF_file, std::string eCorrHltName, std::string eCorrYearTag,
    std::string muTrgSF_file, std::string muCorrHltName,
    std::string tauTrgSF_file,
    std::string eTauTrgSF_file, std::string etauCorrHltName,
    std::string muTauTrgSF_file, std::string muTauCorrHltName,
    std::string taujetTrgSF_file, std::string taujetCorrHltName,
    std::string vbfTrgSF_file, std::string tauCorr_vbfTrg, std::string jetCorr_vbfTrg 
    ) :
    eCorr_eTrg_sf_(eTrgSF_file, "Electron-HLT-SF"),
    eCorr_eTrg_effMC_(eTrgSF_file, "Electron-HLT-McEff"),
    eCorr_eTrg_effData_(eTrgSF_file, "Electron-HLT-DataEff"),
    muCorr_muTrg_sf_(muTrgSF_file, muCorrHltName),
    muCorr_muTrg_effMC_(muTrgSF_file, muCorrHltName+"_MCeff"),
    muCorr_muTrg_effData_(muTrgSF_file, muCorrHltName+"_DATAeff"),
    tauCorr_(tauTrgSF_file, "tauTriggerSF"),
    eCorr_etauTrg_effMC_(eTauTrgSF_file, "Electron-HLT-McEff"),
    eCorr_etauTrg_effData_(eTauTrgSF_file, "Electron-HLT-DataEff"),
    muCorr_mutauTrg_effMC_(muTauTrgSF_file, muTauCorrHltName+"_MCeff"),
    muCorr_mutauTrg_effData_(muTauTrgSF_file, muTauCorrHltName+"_DATAeff"),
    jetCorr_taujetTrg_(taujetTrgSF_file, taujetCorrHltName),
    tauCorr_vbfTrg_(vbfTrgSF_file, tauCorr_vbfTrg),
    jetCorr_vbfTrg_(vbfTrgSF_file, jetCorr_vbfTrg)
{
    eCorrYearTag_ = eCorrYearTag;
    eCorrHltName_ = eCorrHltName;
    etauCorrHltName_ = etauCorrHltName;

    isomuTrg_offMuThr_     = isomuTrg_offMuThr;
    mutauTrg_offMuThr_     = mutauTrg_offMuThr;
    mutauTrg_offTauThr_    = mutauTrg_offTauThr;
    eleTrg_offEleThr_      = eleTrg_offEleThr;
    etauTrg_offEleThr_     = etauTrg_offEleThr;
    etauTrg_offTauThr_     = etauTrg_offTauThr;
    ditauTrg_offTauThr_    = ditauTrg_offTauThr;
    ditaujetTrg_offTauThr_ = ditaujetTrg_offTauThr;
    ditaujetTrg_offJetThr_ = ditaujetTrg_offJetThr;
}

triggerSF_output HHTrigSFinterface_corrlib::get_scale_factors(
    int pairType, bool isVBFtrigger, std::string TauvsJetWP,
    float bjet1_pt, float bjet1_eta,
    int dau1_decayMode, float dau1_pt, float dau1_eta, 
    int dau2_decayMode, float dau2_pt, float dau2_eta, 
    float vbfjet1_pt, float vbfjet1_eta, float vbfjet1_phi, float vbfjet1_mass,
    float vbfjet2_pt, float vbfjet2_eta, float vbfjet2_phi, float vbfjet2_mass)
{
    triggerSF_output outputSF; // final output of the function

    std::vector<double> trigSF, trigSF_tauup, trigSF_taudown;

    if (pairType == 0) {
        // single-cross OR region
        if (fabs(dau1_eta) < 2.1) {
            int passSingle = (dau1_pt > isomuTrg_offMuThr_) ? 1 : 0;
            int passCross  = ((dau1_pt > mutauTrg_offMuThr_) &&
                              (dau2_pt > mutauTrg_offTauThr_)) ? 1 : 0;

            // single lepton trigger
            double effL_Data_nom = 1.0, effL_Data_up = 1.0, effL_Data_down = 1.0, 
                   effL_MC_nom = 1.0, effL_MC_up = 1.0, effL_MC_down = 1.0;
            if (passSingle) {
                effL_Data_nom  = muCorr_muTrg_effData_.eval({fabs(dau1_eta), dau1_pt, "nominal"});
                effL_Data_up   = muCorr_muTrg_effData_.eval({fabs(dau1_eta), dau1_pt, "systup"});
                effL_Data_down = muCorr_muTrg_effData_.eval({fabs(dau1_eta), dau1_pt, "systdown"});
                effL_MC_nom    = muCorr_muTrg_effMC_.eval({fabs(dau1_eta), dau1_pt, "nominal"});
                effL_MC_up     = muCorr_muTrg_effMC_.eval({fabs(dau1_eta), dau1_pt, "systup"});
                effL_MC_down   = muCorr_muTrg_effMC_.eval({fabs(dau1_eta), dau1_pt, "systdown"});
            }
            std::vector<double> effL_Data = {effL_Data_down, effL_Data_nom, effL_Data_up};
            std::vector<double> effL_MC   = {effL_MC_down, effL_MC_nom, effL_MC_up};

            // cross lepton trigger
            double effl_Data_nom = 1.0, effl_Data_up = 1.0, effl_Data_down = 1.0, 
                   effl_MC_nom = 1.0, effl_MC_up = 1.0, effl_MC_down = 1.0,
                   efft_Data_nom = 1.0, efft_MC_nom = 1.0;
            if (passCross){
                // lepton leg
                effl_Data_nom  = muCorr_mutauTrg_effData_.eval({fabs(dau1_eta), dau1_pt, "nominal"});
                effl_Data_up   = muCorr_mutauTrg_effData_.eval({fabs(dau1_eta), dau1_pt, "systup"});
                effl_Data_down = muCorr_mutauTrg_effData_.eval({fabs(dau1_eta), dau1_pt, "systdown"});
                effl_MC_nom    = muCorr_mutauTrg_effMC_.eval({fabs(dau1_eta), dau1_pt, "nominal"});
                effl_MC_up     = muCorr_mutauTrg_effMC_.eval({fabs(dau1_eta), dau1_pt, "systup"});
                effl_MC_down   = muCorr_mutauTrg_effMC_.eval({fabs(dau1_eta), dau1_pt, "systdown"});

                // tau leg
                efft_Data_nom  = tauCorr_.eval({dau2_pt, dau2_decayMode, "mutau", TauvsJetWP, "eff_data", "nom"});
                efft_MC_nom    = tauCorr_.eval({dau2_pt, dau2_decayMode, "mutau", TauvsJetWP, "eff_mc",   "nom"});
            }
            std::vector<double> effl_Data = {effl_Data_down, effl_Data_nom, effl_Data_up};
            std::vector<double> effl_MC   = {effl_MC_down, effl_MC_nom, effl_MC_up};

            // calculate SFs
            std::vector<double> Eff_Data, Eff_MC;
            for (size_t i = 0; i < effl_Data.size(); i++) {
                Eff_Data.push_back(
                    passSingle * effL_Data[i] 
                    - passCross * passSingle * std::min(effl_Data[i], effL_Data[i]) * efft_Data_nom
                    + passCross * effl_Data[i] * efft_Data_nom);
                
                Eff_MC.push_back(
                    passSingle * effL_MC[i] 
                    - passCross * passSingle * std::min(effl_MC[i], effL_MC[i]) * efft_MC_nom
                    + passCross * effl_MC[i] * efft_MC_nom);
                
                trigSF.push_back(Eff_Data[i] / Eff_MC[i]);
            }

            std::vector<double> efft_Data_up(4, efft_Data_nom);
            std::vector<double> efft_Data_down(4, efft_Data_nom);
            std::vector<double> efft_MC_up(4, efft_MC_nom);
            std::vector<double> efft_MC_down(4, efft_MC_nom);
            std::vector<double> Eff_Data_tauup(4, 1.0);
            std::vector<double> Eff_Data_taudown(4, 1.0);
            std::vector<double> Eff_MC_tauup(4, 1.0);
            std::vector<double> Eff_MC_taudown(4, 1.0);
            if (passCross) {
                for (size_t idm = 0; idm < decayModes.size(); idm++) {
                    if (decayModes[idm] == dau2_decayMode) {
                        efft_Data_up[idm]   = tauCorr_.eval({dau2_pt, dau2_decayMode, "mutau", TauvsJetWP, "eff_data", "up"});
                        efft_Data_down[idm] = tauCorr_.eval({dau2_pt, dau2_decayMode, "mutau", TauvsJetWP, "eff_data", "down"});
                        efft_MC_up[idm]     = tauCorr_.eval({dau2_pt, dau2_decayMode, "mutau", TauvsJetWP, "eff_mc",   "up"});
                        efft_MC_down[idm]   = tauCorr_.eval({dau2_pt, dau2_decayMode, "mutau", TauvsJetWP, "eff_mc",   "down"});
                    }
                }
            }

            for (size_t idm = 0; idm < decayModes.size(); idm++) {
                Eff_Data_tauup[idm] = 
                    passSingle * effL_Data[1]
                    - passCross * passSingle * std::min(effl_Data[1], effL_Data[1]) * efft_Data_up[idm]
                    + passCross * effl_Data[1] * efft_Data_up[idm];
                
                Eff_Data_taudown[idm] =
                    passSingle * effL_Data[1] 
                    - passCross * passSingle * std::min(effl_Data[1], effL_Data[1]) * efft_Data_down[idm]
                    + passCross * effl_Data[1] * efft_Data_down[idm];
                
                Eff_MC_tauup[idm] =
                    passSingle * effL_MC[1] 
                    - passCross * passSingle * std::min(effl_MC[1], effL_MC[1]) * efft_MC_up[idm]
                    + passCross * effl_MC[1] * efft_MC_up[idm];
                
                Eff_MC_taudown[idm] =
                    passSingle * effL_MC[1] 
                    - passCross * passSingle * std::min(effl_MC[1], effL_MC[1]) * efft_MC_down[idm]
                    + passCross * effl_MC[1] * efft_MC_down[idm];

                trigSF_tauup.push_back(Eff_Data_tauup[idm] / Eff_MC_tauup[idm]);
                trigSF_taudown.push_back(Eff_Data_taudown[idm] / Eff_MC_taudown[idm]);
            }
        }
        // single only region
        else {
            int passSingle = (dau1_pt > isomuTrg_offMuThr_) ? 1 : 0;
            double SF_nom  = passSingle ? muCorr_muTrg_sf_.eval({fabs(dau1_eta), dau1_pt, "nominal"})  : 1.;
            double SF_up   = passSingle ? muCorr_muTrg_sf_.eval({fabs(dau1_eta), dau1_pt, "systup"})   : 1.;
            double SF_down = passSingle ? muCorr_muTrg_sf_.eval({fabs(dau1_eta), dau1_pt, "systdown"}) : 1.;
            trigSF = {SF_down, SF_nom, SF_up};
            for (size_t idm = 0; idm < decayModes.size(); idm++) {
                trigSF_tauup.push_back(SF_nom);
                trigSF_taudown.push_back(SF_nom);
            }
        }

        // struct {trigSF,
        //         trigSF_muUp, trigSF_muDown, trigSF_eleUp, trigSF_eleDown,
        //         trigSF_DM0Up, trigSF_DM1Up, trigSF_DM10Up, trigSF_DM11Up, 
        //         trigSF_DM0Down, trigSF_DM1Down, trigSF_DM10Down, trigSF_DM11Down,
        //         trigSF_vbfjetUp, trigSF_vbfjetDown, trigSF_jetUp, trigSF_jetDown}
        outputSF.update(trigSF[1],
                        trigSF[2], trigSF[0], 1.0, 1.0,
                        trigSF_tauup[0], trigSF_tauup[1], trigSF_tauup[2], trigSF_tauup[3],
                        trigSF_taudown[0], trigSF_taudown[1], trigSF_taudown[2], trigSF_taudown[3],
                        1.0, 1.0, 1.0, 1.0,
                        trigSF[1]); // JM FIXME: additional copy to allow two values for pairType==2
    }

    else if (pairType == 1) {
        // single-cross OR region
        if (fabs(dau1_eta) < 2.1) {
            int passSingle = (dau1_pt > eleTrg_offEleThr_) ? 1 : 0;
            int passCross  = ((dau1_pt > etauTrg_offEleThr_) && 
                              (dau2_pt > etauTrg_offTauThr_)) ? 1 : 0;

            double effL_Data_nom = 1.0, effL_Data_up = 1.0, effL_Data_down = 1.0, 
                   effL_MC_nom = 1.0, effL_MC_up = 1.0, effL_MC_down = 1.0;
            // single lepton trigger
            if (passSingle) {
                effL_Data_nom  = eCorr_eTrg_effData_.eval({eCorrYearTag_, "nom",  eCorrHltName_, dau1_eta, dau1_pt});
                effL_Data_up   = eCorr_eTrg_effData_.eval({eCorrYearTag_, "up",   eCorrHltName_, dau1_eta, dau1_pt});
                effL_Data_down = eCorr_eTrg_effData_.eval({eCorrYearTag_, "down", eCorrHltName_, dau1_eta, dau1_pt});
                effL_MC_nom    = eCorr_eTrg_effMC_.eval({eCorrYearTag_, "nom",  eCorrHltName_, dau1_eta, dau1_pt});
                effL_MC_up     = eCorr_eTrg_effMC_.eval({eCorrYearTag_, "up",   eCorrHltName_, dau1_eta, dau1_pt});
                effL_MC_down   = eCorr_eTrg_effMC_.eval({eCorrYearTag_, "down", eCorrHltName_, dau1_eta, dau1_pt});
            }
            std::vector<double> effL_Data = {effL_Data_down, effL_Data_nom, effL_Data_up};
            std::vector<double> effL_MC   = {effL_MC_down, effL_MC_nom, effL_MC_up};

            // cross lepton trigger
            double effl_Data_nom = 1.0, effl_Data_up = 1.0, effl_Data_down = 1.0, 
                   effl_MC_nom = 1.0, effl_MC_up = 1.0, effl_MC_down = 1.0,
                   efft_Data_nom = 1.0, efft_MC_nom = 1.0;
            if (passCross){
                // lepton leg
                effl_Data_nom  = eCorr_etauTrg_effData_.eval({eCorrYearTag_, "nom", etauCorrHltName_, dau1_eta, dau1_pt});
                effl_Data_up   = eCorr_etauTrg_effData_.eval({eCorrYearTag_, "up", etauCorrHltName_, dau1_eta, dau1_pt});
                effl_Data_down = eCorr_etauTrg_effData_.eval({eCorrYearTag_, "down", etauCorrHltName_, dau1_eta, dau1_pt});
                effl_MC_nom    = eCorr_etauTrg_effMC_.eval({eCorrYearTag_, "nom", etauCorrHltName_, dau1_eta, dau1_pt});
                effl_MC_up     = eCorr_etauTrg_effMC_.eval({eCorrYearTag_, "up", etauCorrHltName_, dau1_eta, dau1_pt});
                effl_MC_down   = eCorr_etauTrg_effMC_.eval({eCorrYearTag_, "down", etauCorrHltName_, dau1_eta, dau1_pt});

                // tau leg
                efft_Data_nom  = tauCorr_.eval({dau2_pt, dau2_decayMode, "etau", TauvsJetWP, "eff_data", "nom"});
                efft_MC_nom    = tauCorr_.eval({dau2_pt, dau2_decayMode, "etau", TauvsJetWP, "eff_mc", "nom"});
            }
            std::vector<double> effl_Data = {effl_Data_down, effl_Data_nom, effl_Data_up};
            std::vector<double> effl_MC   = {effl_MC_down, effl_MC_nom, effl_MC_up};

            // calculate SFs
            std::vector<double> Eff_Data, Eff_MC;
            for (size_t i = 0; i < effl_Data.size(); i++) {
                Eff_Data.push_back(
                    passSingle * effL_Data[i] 
                    - passCross * passSingle * std::min(effl_Data[i], effL_Data[i]) * efft_Data_nom
                    + passCross * effl_Data[i] * efft_Data_nom);
                
                Eff_MC.push_back(
                    passSingle * effL_MC[i] 
                    - passCross * passSingle * std::min(effl_MC[i], effL_MC[i]) * efft_MC_nom
                    + passCross * effl_MC[i] * efft_MC_nom);
                
                trigSF.push_back(Eff_Data[i] / Eff_MC[i]);
            }

            std::vector<double> efft_Data_up(4, efft_Data_nom);
            std::vector<double> efft_Data_down(4, efft_Data_nom);
            std::vector<double> efft_MC_up(4, efft_MC_nom);
            std::vector<double> efft_MC_down(4, efft_MC_nom);
            std::vector<double> Eff_Data_tauup(4, 1.0);
            std::vector<double> Eff_Data_taudown(4, 1.0);
            std::vector<double> Eff_MC_tauup(4, 1.0);
            std::vector<double> Eff_MC_taudown(4, 1.0);
            if (passCross) {
                for (size_t idm = 0; idm < decayModes.size(); idm++) {
                    if (decayModes[idm] == dau2_decayMode) {
                        efft_Data_up[idm]   = tauCorr_.eval({dau2_pt, dau2_decayMode, "etau", TauvsJetWP, "eff_data", "up"});
                        efft_Data_down[idm] = tauCorr_.eval({dau2_pt, dau2_decayMode, "etau", TauvsJetWP, "eff_data", "down"});
                        efft_MC_up[idm]     = tauCorr_.eval({dau2_pt, dau2_decayMode, "etau", TauvsJetWP, "eff_mc", "up"});
                        efft_MC_down[idm]   = tauCorr_.eval({dau2_pt, dau2_decayMode, "etau", TauvsJetWP, "eff_mc", "down"});
                    }
                }
            }

            for (size_t idm = 0; idm < decayModes.size(); idm++) {
                Eff_Data_tauup[idm] = 
                    passSingle * effL_Data[1]
                    - passCross * passSingle * std::min(effl_Data[1], effL_Data[1]) * efft_Data_up[idm]
                    + passCross * effl_Data[1] * efft_Data_up[idm];
                
                Eff_Data_taudown[idm] =
                    passSingle * effL_Data[1] 
                    - passCross * passSingle * std::min(effl_Data[1], effL_Data[1]) * efft_Data_down[idm]
                    + passCross * effl_Data[1] * efft_Data_down[idm];
                
                Eff_MC_tauup[idm] =
                    passSingle * effL_MC[1] 
                    - passCross * passSingle * std::min(effl_MC[1], effL_MC[1]) * efft_MC_up[idm]
                    + passCross * effl_MC[1] * efft_MC_up[idm];
                
                Eff_MC_taudown[idm] =
                    passSingle * effL_MC[1] 
                    - passCross * passSingle * std::min(effl_MC[1], effL_MC[1]) * efft_MC_down[idm]
                    + passCross * effl_MC[1] * efft_MC_down[idm];

                trigSF_tauup.push_back(Eff_Data_tauup[idm] / Eff_MC_tauup[idm]);
                trigSF_taudown.push_back(Eff_Data_taudown[idm] / Eff_MC_taudown[idm]);
            }
        }
        // single only region
        else {
            int passSingle = (dau1_pt > eleTrg_offEleThr_) ? 1 : 0;
            double SF_nom  = passSingle ? eCorr_eTrg_sf_.eval({eCorrYearTag_, "sf",     eCorrHltName_, dau1_eta, dau1_pt}) : 1.;
            double SF_up   = passSingle ? eCorr_eTrg_sf_.eval({eCorrYearTag_, "sfup",   eCorrHltName_, dau1_eta, dau1_pt}) : 1.;
            double SF_down = passSingle ? eCorr_eTrg_sf_.eval({eCorrYearTag_, "sfdown", eCorrHltName_, dau1_eta, dau1_pt}) : 1.;
            trigSF = {SF_down, SF_nom, SF_up};
            for (size_t idm = 0; idm < decayModes.size(); idm++) {
                trigSF_tauup.push_back(SF_nom);
                trigSF_taudown.push_back(SF_nom);
            }
        }

        // struct {trigSF,
        //         trigSF_muUp, trigSF_muDown, trigSF_eleUp, trigSF_eleDown,
        //         trigSF_DM0Up, trigSF_DM1Up, trigSF_DM10Up, trigSF_DM11Up, 
        //         trigSF_DM0Down, trigSF_DM1Down, trigSF_DM10Down, trigSF_DM11Down,
        //         trigSF_vbfjetUp, trigSF_vbfjetDown, trigSF_jetUp, trigSF_jetDown}
        outputSF.update(trigSF[1],
                        1.0, 1.0, trigSF[2], trigSF[0],
                        trigSF_tauup[0], trigSF_tauup[1], trigSF_tauup[2], trigSF_tauup[3],
                        trigSF_taudown[0], trigSF_taudown[1], trigSF_taudown[2], trigSF_taudown[3],
                        1.0, 1.0, 1.0, 1.0,
                        trigSF[1]); // JM FIXME: additional copy to allow two values for pairType==2
    }

    else if (pairType == 2) {
        if (isVBFtrigger) {
            double SF1 = 1.0; // FIXME: SF to be computed
            double SF2 = 1.0; // FIXME: SF to be computed

            double jetSF = 1.0; // FIXME: SF to be computed
            double jetSF_up = 1.0; // FIXME: SF to be computed
            double jetSF_down = 1.0; // FIXME: SF to be computed

            std::vector<double> SF1_tauup(4, SF1);
            std::vector<double> SF1_taudown(4, SF1);
            std::vector<double> SF2_tauup(4, SF2);
            std::vector<double> SF2_taudown(4, SF2);
            for (size_t idm = 0; idm < decayModes.size(); idm++) {
              if (decayModes[idm] == dau1_decayMode) {
                SF1_tauup[idm]   = 1.0; // FIXME: SF to be computed
                SF1_taudown[idm] = 1.0; // FIXME: SF to be computed
              }
              if (decayModes[idm] == dau2_decayMode) {
                SF2_tauup[idm]   = 1.0; // FIXME: SF to be computed
                SF2_taudown[idm] = 1.0; // FIXME: SF to be computed
              }
            }
            
            trigSF.push_back(jetSF_down * SF1 * SF2);
            trigSF.push_back(jetSF * SF1 * SF2);
            trigSF.push_back(jetSF_up * SF1 * SF2);
            for (size_t idm = 0; idm < decayModes.size(); idm++) {
              trigSF_tauup.push_back(jetSF * SF1_tauup[idm] * SF2_tauup[idm]);
              trigSF_taudown.push_back(jetSF * SF1_taudown[idm] * SF2_taudown[idm]);
            }

            // struct {trigSF,
            //         trigSF_muUp, trigSF_muDown, trigSF_eleUp, trigSF_eleDown,
            //         trigSF_DM0Up, trigSF_DM1Up, trigSF_DM10Up, trigSF_DM11Up, 
            //         trigSF_DM0Down, trigSF_DM1Down, trigSF_DM10Down, trigSF_DM11Down,
            //         trigSF_vbfjetUp, trigSF_vbfjetDown, trigSF_jetUp, trigSF_jetDown}
            outputSF.update(trigSF[1],
                            1.0, 1.0, 1.0, 1.0,
                            trigSF_tauup[0], trigSF_tauup[1], trigSF_tauup[2], trigSF_tauup[3],
                            trigSF_taudown[0], trigSF_taudown[1], trigSF_taudown[2], trigSF_taudown[3],
                            trigSF[2], trigSF[0], 1.0, 1.0,
                            trigSF[1]); // JM FIXME: additional copy to allow two values for pairType==2
        }

        else {
            int passDiTau    = (dau1_pt > ditauTrg_offTauThr_ && dau2_pt > ditauTrg_offTauThr_) ? 1 : 0;
            int passDiTauJet = (dau1_pt > ditaujetTrg_offTauThr_ && dau2_pt > ditaujetTrg_offTauThr_
                                && bjet1_pt > ditaujetTrg_offJetThr_) ? 1 : 0;

            // ditau trigger
            double efftt_Data_nom = 1.0, efftt_MC_nom = 1.0;
            if (passDiTau) {
                efftt_Data_nom  = tauCorr_.eval({dau1_pt, dau1_decayMode, "ditau", TauvsJetWP, "eff_data", "nom"}) 
                                * tauCorr_.eval({dau2_pt, dau2_decayMode, "ditau", TauvsJetWP, "eff_data", "nom"});
                efftt_MC_nom    = tauCorr_.eval({dau1_pt, dau1_decayMode, "ditau", TauvsJetWP, "eff_mc",   "nom"}) 
                                * tauCorr_.eval({dau2_pt, dau2_decayMode, "ditau", TauvsJetWP, "eff_mc",   "nom"});
            }

            std::vector<double> efftt_Data_up(4, 1.0);
            std::vector<double> efftt_Data_down(4, 1.0);
            std::vector<double> efftt_MC_up(4, 1.0);
            std::vector<double> efftt_MC_down(4, 1.0);
            if (passDiTau){
                for (size_t idm = 0; idm < decayModes.size(); idm++) {
                    if (decayModes[idm] == dau1_decayMode) {
                        efftt_Data_up[idm]   *= tauCorr_.eval({dau1_pt, dau1_decayMode, "ditau", TauvsJetWP, "eff_data", "up"});
                        efftt_Data_down[idm] *= tauCorr_.eval({dau1_pt, dau1_decayMode, "ditau", TauvsJetWP, "eff_data", "down"});
                        efftt_MC_up[idm]     *= tauCorr_.eval({dau1_pt, dau1_decayMode, "ditau", TauvsJetWP, "eff_mc",   "up"});
                        efftt_MC_down[idm]   *= tauCorr_.eval({dau1_pt, dau1_decayMode, "ditau", TauvsJetWP, "eff_mc",   "down"});
                    }
                    if (decayModes[idm] == dau2_decayMode) {
                        efftt_Data_up[idm]   *= tauCorr_.eval({dau2_pt, dau2_decayMode, "ditau", TauvsJetWP, "eff_data", "up"});
                        efftt_Data_down[idm] *= tauCorr_.eval({dau2_pt, dau2_decayMode, "ditau", TauvsJetWP, "eff_data", "down"});
                        efftt_MC_up[idm]     *= tauCorr_.eval({dau2_pt, dau2_decayMode, "ditau", TauvsJetWP, "eff_mc",   "up"});
                        efftt_MC_down[idm]   *= tauCorr_.eval({dau2_pt, dau2_decayMode, "ditau", TauvsJetWP, "eff_mc",   "down"});
                    }
                }
            }

            // ditau+jet trigger
            // ditau leg
            double effttj_Data_nom = 1.0, effttj_MC_nom = 1.0;
            if (passDiTauJet) {
                effttj_Data_nom  = tauCorr_.eval({dau1_pt, dau1_decayMode, "ditaujet", TauvsJetWP, "eff_data", "nom"}) 
                                 * tauCorr_.eval({dau2_pt, dau2_decayMode, "ditaujet", TauvsJetWP, "eff_data", "nom"});
                effttj_MC_nom    = tauCorr_.eval({dau1_pt, dau1_decayMode, "ditaujet", TauvsJetWP, "eff_mc", "nom"}) 
                                 * tauCorr_.eval({dau2_pt, dau2_decayMode, "ditaujet", TauvsJetWP, "eff_mc", "nom"});
            }

            std::vector<double> effttj_Data_up(4, 1.0);
            std::vector<double> effttj_Data_down(4, 1.0);
            std::vector<double> effttj_MC_up(4, 1.0);
            std::vector<double> effttj_MC_down(4, 1.0);
            if (passDiTauJet) {
                for (size_t idm = 0; idm < decayModes.size(); idm++) {
                    if (decayModes[idm] == dau1_decayMode) {
                        effttj_Data_up[idm]   *= tauCorr_.eval({dau1_pt, dau1_decayMode, "ditaujet", TauvsJetWP, "eff_data", "up"});
                        effttj_Data_down[idm] *= tauCorr_.eval({dau1_pt, dau1_decayMode, "ditaujet", TauvsJetWP, "eff_data", "down"});
                        effttj_MC_up[idm]     *= tauCorr_.eval({dau1_pt, dau1_decayMode, "ditaujet", TauvsJetWP, "eff_mc",   "up"});
                        effttj_MC_down[idm]   *= tauCorr_.eval({dau1_pt, dau1_decayMode, "ditaujet", TauvsJetWP, "eff_mc",   "down"});
                    }
                    if (decayModes[idm] == dau2_decayMode) {
                        effttj_Data_up[idm]   *= tauCorr_.eval({dau2_pt, dau2_decayMode, "ditaujet", TauvsJetWP, "eff_data", "up"});
                        effttj_Data_down[idm] *= tauCorr_.eval({dau2_pt, dau2_decayMode, "ditaujet", TauvsJetWP, "eff_data", "down"});
                        effttj_MC_up[idm]     *= tauCorr_.eval({dau2_pt, dau2_decayMode, "ditaujet", TauvsJetWP, "eff_mc",   "up"});
                        effttj_MC_down[idm]   *= tauCorr_.eval({dau2_pt, dau2_decayMode, "ditaujet", TauvsJetWP, "eff_mc",   "down"});
                    }
                }
            }

            // jet leg
            double effj_Data_nom  = jetCorr_taujetTrg_.eval({bjet1_pt, abs(bjet1_eta), "nom", "data"});
            double effj_Data_up   = jetCorr_taujetTrg_.eval({bjet1_pt, abs(bjet1_eta), "up", "data"});
            double effj_Data_down = jetCorr_taujetTrg_.eval({bjet1_pt, abs(bjet1_eta), "down", "data"}); 
            double effj_MC_nom    = jetCorr_taujetTrg_.eval({bjet1_pt, abs(bjet1_eta), "nom", "mc"}); 
            double effj_MC_up     = jetCorr_taujetTrg_.eval({bjet1_pt, abs(bjet1_eta), "up", "mc"}); 
            double effj_MC_down   = jetCorr_taujetTrg_.eval({bjet1_pt, abs(bjet1_eta), "down", "mc"});
            std::vector<double> effj_Data = {effj_Data_down, effj_Data_nom, effj_Data_up};
            std::vector<double> effj_MC = {effj_MC_down, effj_MC_nom, effj_MC_up};

            // calculate SFs
            std::vector<double> Eff_Data, Eff_MC;
            std::vector<double> trigSF_nojetSF, Eff_Data_nojetSF, Eff_MC_nojetSF; // JM FIXME: temporary additional trigSF without jet leg SFs
            for (size_t i = 0; i < effj_Data.size(); i++) {
                Eff_Data.push_back(
                    passDiTau * efftt_Data_nom
                    - passDiTau * passDiTauJet * std::min(effttj_Data_nom, efftt_Data_nom) * effj_Data[i]
                    + passDiTauJet * effttj_Data_nom * effj_Data[i]);
                
                Eff_MC.push_back(
                    passDiTau * efftt_MC_nom
                    - passDiTau * passDiTauJet * std::min(effttj_MC_nom, efftt_MC_nom) * effj_MC[i]
                    + passDiTauJet * effttj_MC_nom * effj_MC[i]);
                
                trigSF.push_back(Eff_Data[i] / Eff_MC[i]);

                Eff_Data_nojetSF.push_back(
                    passDiTau * efftt_Data_nom
                    - passDiTau * passDiTauJet * std::min(effttj_Data_nom, efftt_Data_nom) * 1.0
                    + passDiTauJet * effttj_Data_nom * 1.0);
                
                Eff_MC_nojetSF.push_back(
                    passDiTau * efftt_MC_nom
                    - passDiTau * passDiTauJet * std::min(effttj_MC_nom, efftt_MC_nom) * 1.0
                    + passDiTauJet * effttj_MC_nom * 1.0);
                
                trigSF_nojetSF.push_back(Eff_Data_nojetSF[i] / Eff_MC_nojetSF[i]);
            }

            std::vector<double> Eff_Data_tauup(4, 1.0);
            std::vector<double> Eff_Data_taudown(4, 1.0);
            std::vector<double> Eff_MC_tauup(4, 1.0);
            std::vector<double> Eff_MC_taudown(4, 1.0);
            for (size_t idm = 0; idm < decayModes.size(); idm++) {
                Eff_Data_tauup[idm] = 
                    passDiTau * efftt_Data_up[idm]
                    - passDiTau * passDiTauJet * std::min(effttj_Data_up[idm], efftt_Data_up[idm]) * effj_Data[1]
                    + passDiTauJet * effttj_Data_up[idm] * effj_Data[1];

                Eff_Data_taudown[idm] = 
                    passDiTau * efftt_Data_down[idm]
                    - passDiTau * passDiTauJet * std::min(effttj_Data_down[idm], efftt_Data_down[idm]) * effj_Data[1]
                    + passDiTauJet * effttj_Data_down[idm] * effj_Data[1];

                Eff_MC_tauup[idm] = 
                    passDiTau * efftt_MC_up[idm]
                    - passDiTau * passDiTauJet * std::min(effttj_MC_up[idm], efftt_MC_up[idm]) * effj_MC[1]
                    + passDiTauJet * effttj_MC_up[idm] * effj_MC[1];

                Eff_MC_taudown[idm] = 
                    passDiTau * efftt_MC_down[idm]
                    - passDiTau * passDiTauJet * std::min(effttj_MC_down[idm], efftt_MC_down[idm]) * effj_MC[1]
                    + passDiTauJet * effttj_MC_down[idm] * effj_MC[1];

                trigSF_tauup.push_back(Eff_Data_tauup[idm] / Eff_MC_tauup[idm]);
                trigSF_taudown.push_back(Eff_Data_taudown[idm] / Eff_MC_taudown[idm]);
            }

            // struct {trigSF,
            //         trigSF_muUp, trigSF_muDown, trigSF_eleUp, trigSF_eleDown,
            //         trigSF_DM0Up, trigSF_DM1Up, trigSF_DM10Up, trigSF_DM11Up, 
            //         trigSF_DM0Down, trigSF_DM1Down, trigSF_DM10Down, trigSF_DM11Down,
            //         trigSF_vbfjetUp, trigSF_vbfjetDown, trigSF_jetUp, trigSF_jetDown}
            outputSF.update(trigSF[1],
                            1.0, 1.0, 1.0, 1.0,
                            trigSF_tauup[0], trigSF_tauup[1], trigSF_tauup[2], trigSF_tauup[3],
                            trigSF_taudown[0], trigSF_taudown[1], trigSF_taudown[2], trigSF_taudown[3],
                            1.0, 1.0, trigSF[2], trigSF[0],
                            trigSF_nojetSF[1]); // JM FIXME: temporary additional trigSF without jet leg SFs
        }
    
    } 

    else if (pairType == 3) { 
        double SF = 1., SF_up = 1., SF_down = 1.;
        float high_pt  = (dau1_pt >= dau2_pt) ? dau1_pt : dau2_pt;
        float high_eta = (dau1_pt >= dau2_pt) ? dau1_eta : dau2_eta;

        if (high_pt > isomuTrg_offMuThr_){
            SF      = muCorr_muTrg_sf_.eval({fabs(high_eta), high_pt, "nominal"});
            SF_up   = muCorr_muTrg_sf_.eval({fabs(high_eta), high_pt, "systup"});
            SF_down = muCorr_muTrg_sf_.eval({fabs(high_eta), high_pt, "systdown"});
        }

        outputSF.update(SF, SF_up, SF_down, SF_up,
                        SF_down, 1., 1., 1., 1., 1., 1.,
                        1., 1., 1., 1.,1., 1.,
                        SF); // JM FIXME: additional copy to allow two values for pairType==2
    }

    else if (pairType == 4) {
        double SF = 1., SF_up = 1., SF_down = 1.;
        float high_pt  = (dau1_pt >= dau2_pt) ? dau1_pt : dau2_pt;
        float high_eta = (dau1_pt >= dau2_pt) ? dau1_eta : dau2_eta;

        if (high_pt > eleTrg_offEleThr_){
            SF      = eCorr_eTrg_sf_.eval({eCorrYearTag_, "sf",     eCorrHltName_, high_eta, high_pt});
            SF_up   = eCorr_eTrg_sf_.eval({eCorrYearTag_, "sfup",   eCorrHltName_, high_eta, high_pt});
            SF_down = eCorr_eTrg_sf_.eval({eCorrYearTag_, "sfdown", eCorrHltName_, high_eta, high_pt});
        }

        outputSF.update(SF, SF_up, SF_down, 1., 1., SF_up,
                        SF_down, 1., 1., 1., 1.,
                        1., 1., 1., 1.,1., 1.,
                        SF); // JM FIXME: additional copy to allow two values for pairType==2
    }

    else if (pairType == 5) {
        int passMu  = (dau1_pt > isomuTrg_offMuThr_) ? 1 : 0;
        int passEle = (dau2_pt > eleTrg_offEleThr_) ? 1 : 0;

        double SFmu      = passMu ? muCorr_muTrg_sf_.eval({fabs(dau1_eta), dau1_pt, "nominal"})  : 1.;
        double SFmu_up   = passMu ? muCorr_muTrg_sf_.eval({fabs(dau1_eta), dau1_pt, "systup"})   : 1.;
        double SFmu_down = passMu ? muCorr_muTrg_sf_.eval({fabs(dau1_eta), dau1_pt, "systdown"}) : 1.;

        double SFe      = passEle ? eCorr_eTrg_sf_.eval({eCorrYearTag_, "sf",     eCorrHltName_, dau2_eta, dau2_pt}) : 1.;
        double SFe_up   = passEle ? eCorr_eTrg_sf_.eval({eCorrYearTag_, "sfup",   eCorrHltName_, dau2_eta, dau2_pt}) : 1.;
        double SFe_down = passEle ? eCorr_eTrg_sf_.eval({eCorrYearTag_, "sfdown", eCorrHltName_, dau2_eta, dau2_pt}) : 1.;

        std::vector<double> trigSF  = {
            SFe * SFmu - sqrt( (SFmu - SFmu_down) * (SFmu - SFmu_down) + (SFe - SFe_down) * (SFe - SFe_down) ),
            SFe * SFmu,
            SFe * SFmu + sqrt( (SFmu - SFmu_up) * (SFmu - SFmu_up) + (SFe - SFe_up) * (SFe - SFe_up) )
        };

        std::vector<double> trigSF_mu  = {SFmu_down,
                                          SFmu,
                                          SFmu_up};
        
        std::vector<double> trigSF_ele = {SFe_down,
                                          SFe,
                                          SFe_up};

        outputSF.update(trigSF[1], trigSF[2], trigSF[0],
                        trigSF_mu[2], trigSF_mu[0], trigSF_ele[2], trigSF_ele[0],
                        1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
                        trigSF[1]); // JM FIXME: additional copy to allow two values for pairType==2
    }

    return outputSF;
}

triggerSF_output HHTrigSFinterface_corrlib::get_hh_trigsf(
    int pairType, bool isVBFtrigger, int bjet1_idx, int fatjet_idx,
    int dau1_index, int dau2_index, int vbfjet1_index, int vbfjet2_index,
    fRVec muon_pt, fRVec muon_eta, fRVec electron_pt, fRVec electron_eta,
    fRVec tau_pt, fRVec tau_eta, iRVec tau_decayMode, std::string tau_vsJetWP,
    fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass,
    fRVec fatjet_pt, fRVec fatjet_eta)
{
    if (dau1_index < 0 || dau2_index < 0) {
        triggerSF_output outputSF;
        return outputSF;
    }

    std::vector<int> dummyMuVecI(muon_pt.size(), -999);
    std::vector<int> dummyEleVecI(electron_pt.size(), -999);
    std::vector<int> dummyTauVecI(tau_pt.size(), -999);
    std::vector<float> dummyMuVecF(muon_pt.size(), -999.);
    std::vector<float> dummyEleVecF(electron_pt.size(), -999.);
    std::vector<float> dummyTauVecF(tau_pt.size(), -999.);
    leps lptns = GetLeps(muon_pt, muon_eta, dummyMuVecF, dummyMuVecF, dummyMuVecI,
                         electron_pt, electron_eta, dummyEleVecF, dummyEleVecF, dummyEleVecI,
                         tau_pt, tau_eta, dummyTauVecF, dummyTauVecF,  dummyTauVecI, tau_decayMode,
                         dummyTauVecF, dummyTauVecF, dummyTauVecF,
                         dau1_index, dau2_index, pairType);

    float bjet1_pt=-999, bjet1_eta=-999;
    if (bjet1_idx >= 0) {
        bjet1_pt  = jet_pt.at(bjet1_idx);
        bjet1_eta = jet_eta.at(bjet1_idx);
    }
    else if (fatjet_idx >= 0) {
        bjet1_pt  = fatjet_pt.at(fatjet_idx);
        bjet1_eta = fatjet_eta.at(fatjet_idx); 
    }

    float vbfjet1_pt=-999, vbfjet1_eta=-999, vbfjet1_phi=-999, vbfjet1_mass=-999;
    float vbfjet2_pt=-999, vbfjet2_eta=-999, vbfjet2_phi=-999, vbfjet2_mass=-999;
    if (vbfjet1_index >= 0 && vbfjet2_index >= 0) {
        vbfjet1_pt = jet_pt.at(vbfjet1_index);
        vbfjet1_eta = jet_eta.at(vbfjet1_index);
        vbfjet1_phi = jet_phi.at(vbfjet1_index);
        vbfjet1_mass = jet_mass.at(vbfjet1_index);
        vbfjet2_pt = jet_pt.at(vbfjet2_index);
        vbfjet2_eta = jet_eta.at(vbfjet2_index);
        vbfjet2_phi = jet_phi.at(vbfjet2_index);
        vbfjet2_mass = jet_mass.at(vbfjet2_index);
    }

    return get_scale_factors(pairType, isVBFtrigger, tau_vsJetWP,
                             bjet1_pt, bjet1_eta,
                             lptns.dau1_DM, lptns.dau1_pt, lptns.dau1_eta,
                             lptns.dau2_DM, lptns.dau2_pt, lptns.dau2_eta,
                             vbfjet1_pt, vbfjet1_eta, vbfjet1_phi, vbfjet1_mass,
                             vbfjet2_pt, vbfjet2_eta, vbfjet2_phi, vbfjet2_mass);
}

HHTrigSFinterface_corrlib::~HHTrigSFinterface_corrlib() {}
