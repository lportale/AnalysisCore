#include "Tools/Tools/interface/HHLeptonInterface.h"

// Constructors

HHLeptonInterface::HHLeptonInterface (
    int vvvl_vsjet, int vl_vse, int vvl_vse, int t_vsmu, int vl_vsmu, bool applyOflnIso) {
  vvvl_vsjet_ = vvvl_vsjet;
  vl_vse_ = vl_vse;
  vvl_vse_ = vvl_vse;
  t_vsmu_ = t_vsmu;
  vl_vsmu_ = vl_vsmu;
  applyOflnIso_ = applyOflnIso;
};

// Destructor
HHLeptonInterface::~HHLeptonInterface() {}

// Select tautau pair and assess channel:
//  if   (vMu >= 1  ||  vEle >= 1)  -->  -1 (don't pass veto)
//  elif (tMu == 1  &&  tEle == 0)  -->  0 MuTau
//  elif (tMu == 0  &&  tEle == 1)  -->  1 ETau
//  elif (tMu == 0  &&  tEle == 0)  -->  2 TauTau
//  elif (tMu == 2  &&  tEle == 0)  -->  3 MuMu
//  elif (tMu == 0  &&  tEle == 2)  -->  4 EE
//  elif (tMu == 1  &&  tEle == 1)  -->  5 EMu
//  else                            --> -1 (no good pair found)
lepton_output HHLeptonInterface::get_dau_indexes(
    fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
    fRVec Muon_pfRelIso04_all, fRVec Muon_dxy, fRVec Muon_dz,
    bRVec Muon_mediumId, bRVec Muon_tightId, iRVec Muon_charge,
    fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
    bRVec Electron_mvaIso_WP80, bRVec Electron_mvaNoIso_WP90,
    bRVec Electron_mvaIso_WP90, fRVec Electron_pfRelIso03_all,
    fRVec Electron_dxy, fRVec Electron_dz, iRVec Electron_charge, fRVec Electron_mvaIso,
    fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
    iRVec Tau_tauIdVSmu, iRVec Tau_tauIdVSe,
    iRVec Tau_tauIdVSjet, fRVec Tau_rawTauIdVSjet,
    fRVec Tau_dz, iRVec Tau_decayMode, iRVec Tau_charge)
{
    // Declare output
    lepton_output evt_out({-1, -1, -1, -1});

    // Select good muons with tight (MuTau events) and loose selections (MuMu events)
    std::vector<int> tightmuons;
    std::vector<int> vetomuons;
    for (size_t imuon = 0; imuon < Muon_pt.size(); imuon ++)
    {
        if (applyOflnIso_ && (fabs(Muon_dxy[imuon]) >= 0.045 || fabs(Muon_dz[imuon]) >= 0.2))
            continue;

        if (applyOflnIso_)
        {
            if (fabs(Muon_eta[imuon]) <= 2.4 && Muon_pfRelIso04_all[imuon] < 0.15 && Muon_pt[imuon] >= 20 && Muon_tightId[imuon])
                tightmuons.push_back(imuon);

            else if (fabs(Muon_eta[imuon]) <= 2.4 && Muon_pfRelIso04_all[imuon] < 0.30 && Muon_pt[imuon] >= 10 && (Muon_tightId[imuon] || Muon_mediumId[imuon]) )
                vetomuons.push_back(imuon);
        }
        else
        {
            if (Muon_pt[imuon] >= 20)
                tightmuons.push_back(imuon);
            else if (Muon_pt[imuon] >= 10)
                vetomuons.push_back(imuon);
        }
    }

    // Select good electrons with tight (ETau events) and loose selections (EE events)
    std::vector<int> tightelectrons;
    std::vector<int> vetoelectrons;
    for (size_t iele = 0; iele < Electron_pt.size(); iele ++)
    {
        if (applyOflnIso_ && (fabs(Electron_dxy[iele]) >= 0.045 || fabs(Electron_dz[iele]) >= 0.2))
            continue;

        if (applyOflnIso_)
        {
            if (fabs(Electron_eta[iele]) <= 2.5 && Electron_pt[iele] >= 24 && Electron_mvaIso_WP80[iele])
                tightelectrons.push_back(iele);

            // mvaNoIso is currently broken in nanoV12 --> to be uncommented once we move to new version of samples
            else if (fabs(Electron_eta[iele]) <= 2.5 && Electron_pt[iele] >= 10 && (Electron_mvaIso_WP90[iele] /* || (Electron_mvaNoIso_WP90[iele] && Electron_pfRelIso03_all[iele] <= 0.3)*/))
                vetoelectrons.push_back(iele);
        }
        else
        {
            if (Electron_pt[iele] >= 24)
                tightelectrons.push_back(iele);
            else if (Electron_pt[iele] >= 10)
                vetoelectrons.push_back(iele);
        }
    }

    // 3rd lepton veto
    if (vetomuons.size() >= 1 || vetoelectrons.size() >= 1)
    {
        // There is at least one additional (veto) muon or electron
        // --> event should be discarded
        return evt_out;
    }

    // 0 - Start MuTau channel
    else if (tightmuons.size() == 1 && tightelectrons.size() == 0)
    {
        // Select good taus with loose pT cut (20 GeV)
        std::vector<int> goodtaus;
        for (size_t itau = 0; itau < Tau_pt.size(); itau ++)
        {
            if (applyOflnIso_)
            {
                if (Tau_tauIdVSmu[itau] < t_vsmu_ || Tau_tauIdVSe[itau] < vl_vse_ || Tau_tauIdVSjet[itau] < vvvl_vsjet_)
                    continue;
                if (fabs(Tau_dz[itau]) > 0.2)
                    continue;
                if (Tau_decayMode[itau] != 0 && Tau_decayMode[itau] != 1 &&
                    Tau_decayMode[itau] != 10 && Tau_decayMode[itau] != 11)
                    continue;
                if (fabs(Tau_eta[itau]) >= 2.5)
                    continue;
                if (Tau_pt[itau] <= 20.)
                    continue;
            }
            goodtaus.push_back(itau);
        }

        // MuTau pairs
        if (goodtaus.size() >= 1)
        {
            std::vector<tau_pair> mutau_pairs;
            for (auto & imuon: tightmuons)
            {
                auto muon_tlv = TLorentzVector();
                muon_tlv.SetPtEtaPhiM(Muon_pt[imuon], Muon_eta[imuon], Muon_phi[imuon], Muon_mass[imuon]);

                for (auto & itau: goodtaus)
                {
                    auto tau_tlv = TLorentzVector();
                    tau_tlv.SetPtEtaPhiM(Tau_pt[itau], Tau_eta[itau], Tau_phi[itau], Tau_mass[itau]);

                    if (tau_tlv.DeltaR(muon_tlv) < 0.5)
                        continue;

                    mutau_pairs.push_back(tau_pair(
                        {imuon, Muon_pfRelIso04_all[imuon], Muon_pt[imuon],
                         itau, Tau_rawTauIdVSjet[itau], Tau_pt[itau]}
                    ));
                }
            }

            // Sort MuTau pairs and return the first one
            if (mutau_pairs.size() > 0)
            {
                std::stable_sort(mutau_pairs.begin(), mutau_pairs.end(), pairSortHybrid);
                int ind1 = mutau_pairs[0].index1;
                int ind2 = mutau_pairs[0].index2;
                int isOS = (int) (Muon_charge[ind1] != Tau_charge[ind2]);

                evt_out.update(0, ind1, ind2, isOS);
            }
        }
    } // end MuTau channel

    // 1 - Start ETau channel
    else if (tightmuons.size() == 0 && tightelectrons.size() == 1)
    {
        // Select good taus with loose pT cut (20 GeV)
        std::vector<int> goodtaus;
        for (size_t itau = 0; itau < Tau_pt.size(); itau ++)
        {
            if (applyOflnIso_)
            {
                if (Tau_tauIdVSmu[itau] < t_vsmu_ || Tau_tauIdVSe[itau] < vl_vse_ || Tau_tauIdVSjet[itau] < vvvl_vsjet_)
                    continue;
                if (fabs(Tau_dz[itau]) > 0.2)
                    continue;
                if (Tau_decayMode[itau] != 0 && Tau_decayMode[itau] != 1 &&
                    Tau_decayMode[itau] != 10 && Tau_decayMode[itau] != 11)
                    continue;
                if (fabs(Tau_eta[itau]) >= 2.5)
                    continue;
                if (Tau_pt[itau] <= 20.)
                    continue;
            }
            goodtaus.push_back(itau);
        }

        // ETau pairs
        if (goodtaus.size() >= 1)
        {
            std::vector<tau_pair> etau_pairs;
            for (auto & iele: tightelectrons)
            {
                auto electron_tlv = TLorentzVector();
                electron_tlv.SetPtEtaPhiM(Electron_pt[iele], Electron_eta[iele],
                                          Electron_phi[iele], Electron_mass[iele]);

                for (auto & itau: goodtaus)
                {
                    auto tau_tlv = TLorentzVector();
                    tau_tlv.SetPtEtaPhiM(Tau_pt[itau], Tau_eta[itau], Tau_phi[itau], Tau_mass[itau]);

                    if (tau_tlv.DeltaR(electron_tlv) < 0.5)
                        continue;

                    etau_pairs.push_back(tau_pair(
                        {iele, Electron_pfRelIso03_all[iele], Electron_pt[iele],
                         itau, Tau_rawTauIdVSjet[itau], Tau_pt[itau]}
                    ));
                }
            }

            // Sort ETau pairs and return the first one
            if (etau_pairs.size() > 0)
            {
                std::stable_sort(etau_pairs.begin(), etau_pairs.end(), pairSortHybrid);
                int ind1 = etau_pairs[0].index1;
                int ind2 = etau_pairs[0].index2;
                int isOS = (int) (Electron_charge[ind1] != Tau_charge[ind2]);

                evt_out.update(1, ind1, ind2, isOS);
            }
        }
    } // end ETau channel

    // 2 - Start TauTau channel
    else if (tightmuons.size() == 0 && tightelectrons.size() == 0)
    {
        // Select good taus
        std::vector<int> goodtaus;
        for (size_t itau = 0; itau < Tau_pt.size(); itau ++)
        {
            if (applyOflnIso_)
            {
                if (Tau_tauIdVSmu[itau] < vl_vsmu_ || Tau_tauIdVSe[itau] < vvl_vse_ || Tau_tauIdVSjet[itau] < vvvl_vsjet_)
                    continue;
                if (fabs(Tau_dz[itau]) > 0.2)
                    continue;
                if (Tau_decayMode[itau] != 0 && Tau_decayMode[itau] != 1 &&
                    Tau_decayMode[itau] != 10 && Tau_decayMode[itau] != 11)
                    continue;
                if (fabs(Tau_eta[itau]) >= 2.5)
                    continue;
                if (Tau_pt[itau] <= 20.)
                    continue;
            }
            goodtaus.push_back(itau);
        }

        // TauTau pairs
        std::vector<tau_pair> tautau_pairs;
        for (auto & itau1 : goodtaus)
        {
            auto tau1_tlv = TLorentzVector();
            tau1_tlv.SetPtEtaPhiM(Tau_pt[itau1], Tau_eta[itau1], Tau_phi[itau1], Tau_mass[itau1]);

            for (auto & itau2 : goodtaus)
            {
                if (itau1 == itau2)
                    continue;

                auto tau2_tlv = TLorentzVector();
                tau2_tlv.SetPtEtaPhiM(Tau_pt[itau2], Tau_eta[itau2], Tau_phi[itau2], Tau_mass[itau2]);

                if (tau1_tlv.DeltaR(tau2_tlv) < 0.5)
                    continue;

                tautau_pairs.push_back(tau_pair(
                    {itau1, Tau_rawTauIdVSjet[itau1], Tau_pt[itau1],
                     itau2, Tau_rawTauIdVSjet[itau2], Tau_pt[itau2],
                    }
                ));
            }
        }

        // Sort TauTau pairs and return the first one
        if (tautau_pairs.size() > 0)
        {
            std::stable_sort(tautau_pairs.begin(), tautau_pairs.end(), pairSortMVA);
            int ind1 = tautau_pairs[0].index1;
            int ind2 = tautau_pairs[0].index2;
            int isOS = (int) (Tau_charge[ind1] != Tau_charge[ind2]);

            evt_out.update(2, ind1, ind2, isOS);
        }
    } // end TauTau channel

    // 3 - Start MuMu channel
    else if (tightmuons.size() == 2 && tightelectrons.size() == 0)
    {
        // MuMu pairs
        std::vector<tau_pair> mumu_pairs;
        for (auto & i : tightmuons)
        {
            for (auto & j : tightmuons)
            {
                auto mu1_tlv = TLorentzVector();
                mu1_tlv.SetPtEtaPhiM(Muon_pt[i], Muon_eta[i], Muon_phi[i], Muon_mass[i]);
                auto mu2_tlv = TLorentzVector();
                mu2_tlv.SetPtEtaPhiM(Muon_pt[j], Muon_eta[j], Muon_phi[j], Muon_mass[j]);

                if (mu1_tlv.DeltaR(mu2_tlv) < 0.5)
                    continue;

                mumu_pairs.push_back(tau_pair(
                    {int(i), Muon_pfRelIso04_all[i], Muon_pt[i],
                     int(j), Muon_pfRelIso04_all[j], Muon_pt[j]}
                ));
            }
        }

        // Sort MuMu pairs and return the first one
        if (mumu_pairs.size() > 0)
        {
            std::stable_sort(mumu_pairs.begin(), mumu_pairs.end(), pairSortRawIso);
            int ind1 = mumu_pairs[0].index1;
            int ind2 = mumu_pairs[0].index2;
            int isOS = (int) (Muon_charge[ind1] != Muon_charge[ind2]);

            evt_out.update(3, ind1, ind2, isOS);
        }
    } // end MuMu channel

    // 4 - Start EE channel
    else if (tightmuons.size() == 0 && tightelectrons.size() == 2)
    {
        // EE pairs
        std::vector<tau_pair> ee_pairs;
        for (auto & i : tightelectrons)
        {
            for (auto & j : tightelectrons)
            {
                auto ele1_tlv = TLorentzVector();
                ele1_tlv.SetPtEtaPhiM(Electron_pt[i], Electron_eta[i], Electron_phi[i], Electron_mass[i]);
                auto ele2_tlv = TLorentzVector();
                ele2_tlv.SetPtEtaPhiM(Electron_pt[j], Electron_eta[j], Electron_phi[j], Electron_mass[j]);

                if (ele1_tlv.DeltaR(ele2_tlv) < 0.5)
                    continue;

                ee_pairs.push_back(tau_pair(
                    {int(i), Electron_pfRelIso03_all[i], Electron_pt[i],
                     int(j), Electron_pfRelIso03_all[j], Electron_pt[j]}
                ));
            }
        }

        // Sort EE pairs and return the first one
        if (ee_pairs.size() > 0)
        {
            std::stable_sort(ee_pairs.begin(), ee_pairs.end(), pairSortRawIso);
            int ind1 = ee_pairs[0].index1;
            int ind2 = ee_pairs[0].index2;
            int isOS = (int) (Electron_charge[ind1] != Electron_charge[ind2]);

            evt_out.update(4, ind1, ind2, isOS);
        }
    } // end EE channel

    // 5 - Start EMu channel
    else if (tightmuons.size() == 1 && tightelectrons.size() == 1)
    {
        // EMu pairs
        std::vector<tau_pair> emu_pairs;
        for (auto & i : tightmuons)
        {
            for (auto & j : tightelectrons)
            {
                auto mu_tlv = TLorentzVector();
                mu_tlv.SetPtEtaPhiM(Muon_pt[i], Muon_eta[i], Muon_phi[i], Muon_mass[i]);
                auto ele_tlv = TLorentzVector();
                ele_tlv.SetPtEtaPhiM(Electron_pt[j], Electron_eta[j], Electron_phi[j], Electron_mass[j]);

                if (mu_tlv.DeltaR(ele_tlv) < 0.5)
                    continue;

                emu_pairs.push_back(tau_pair(
                    {int(i), Muon_pfRelIso04_all[i], Muon_pt[i],
                     int(j), Electron_pfRelIso03_all[j], Electron_pt[j]}
                ));
            }
        }

        // Sort EMu pairs and return the first one
        if (emu_pairs.size() > 0)
        {
            std::stable_sort(emu_pairs.begin(), emu_pairs.end(), pairSortRawIso);
            int ind1 = emu_pairs[0].index1;
            int ind2 = emu_pairs[0].index2;
            int isOS = (int) (Muon_charge[ind1] != Electron_charge[ind2]);

            evt_out.update(5, ind1, ind2, isOS);
        }
    } // end EMu channel

    // Final return
    return evt_out;
}
