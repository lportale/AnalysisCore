#include "Tools/Tools/interface/HHUtils.h"

//------------------------------------------------------------------------
// stuctures and functions needed for trigger checking and matching
bool match_trigger_object(float off_eta, float off_phi, int obj_id,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<int> bits, std::vector<int> addbits = (std::vector<int>) {}, int off_id = 0)
{
  for (size_t iobj = 0; iobj < TrigObj_id.size(); iobj++) {
    if (TrigObj_id[iobj] != obj_id) continue;
    auto const dPhi(std::abs(reco::deltaPhi(off_phi, TrigObj_phi[iobj])));
    auto const dEta(std::abs(off_eta - TrigObj_eta[iobj]));
    auto const delR2(dPhi * dPhi + dEta * dEta);
    if (delR2 > 0.5 * 0.5)
      continue;
    bool matched_bits = true;
    for (auto & bit : bits) {
      if ((TrigObj_filterBits[iobj] & (1<<bit)) == 0) {
        matched_bits = false;
        break;
      }
    }

    for (auto & bit : addbits) {
      if (obj_id != off_id) continue;
      if ((TrigObj_filterBits[iobj] & (1<<bit)) == 0) {
        matched_bits = false;
        break;
      }
    }

    if (!matched_bits)
      continue;
    return true;
  }
  return false;
}

// This version of function is used for single- & cross-lepton triggers
bool pass_trigger(std::vector<TLorentzVector> off_objs, 
    std::vector<trig_req> triggers, std::string dataset, bool isMC,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi)
{
    for (auto &trigger: triggers) {
        if (!trigger.pass)
            continue;

        int passObjs = 0;
        int totObjs = int( trigger.id.size() );

        for (int idx = 0; idx < totObjs; ++idx) {
            if ((off_objs.at(idx).Pt() < trigger.pt.at(idx) || 
                  abs(off_objs.at(idx).Eta()) > trigger.eta.at(idx)))
                continue;

            if (!match_trigger_object(off_objs.at(idx).Eta(), off_objs.at(idx).Phi(), trigger.id.at(idx),
                TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, trigger.bits.at(idx)))
                continue;

            passObjs += 1;
        }

        if (passObjs == totObjs &&
            (isMC || dataset.find(trigger.dataset) != std::string::npos) )
            return true;
    }
    return false;
}

// this is version for offline objects
bool pass_trigger(
    std::vector<TLorentzVector> off_objs, std::vector<int> off_ids,
    std::vector<trig_req> triggers, std::string dataset, bool isMC,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi)
{
    // combine off_objs and off_ids
    std::vector<std::pair<TLorentzVector, int>> combined;
    for (size_t i = 0; i < off_objs.size(); ++i) {
        combined.emplace_back(off_objs[i], off_ids[i]);
    }

    // sort both depending on off_objs Pt()
    std::sort(combined.begin(), combined.end(), [](const std::pair<TLorentzVector, int>& a, const std::pair<TLorentzVector, int>& b) {
        return a.first.Pt() > b.first.Pt();
    });

    // fill back the vectors now both ordered
    for (size_t i = 0; i < combined.size(); ++i) {
        off_objs[i] = combined[i].first;
        off_ids[i] = combined[i].second;
    }

    for (auto &trigger: triggers) {
        if (!trigger.pass)
            continue;

        int passObjs = 0;
        int totObjs = int( trigger.id.size() );

        for (int idx = 0; idx < totObjs; ++idx) {
            if ((off_objs.at(idx).Pt() < trigger.pt.at(idx) || 
                 abs(off_objs.at(idx).Eta()) > trigger.eta.at(idx)))
                continue;

            if (!match_trigger_object(off_objs.at(idx).Eta(), off_objs.at(idx).Phi(), trigger.id.at(idx),
                TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, trigger.bits.at(idx),
                trigger.addbits.at(idx), off_ids.at(idx)))
                continue;

            passObjs += 1;
        }

        if (passObjs == totObjs &&
            (isMC || dataset.find(trigger.dataset) != std::string::npos) )
            return true;
    }
    return false;
}


// // This version of function is used for jet triggers where additional info is necessary
// trigger_hlt_matched_output pass_trigger(
//     bool applyOfln, std::vector<TLorentzVector> mandatory_objs, std::vector<TLorentzVector> optional_objs, 
//     std::vector<trig_req> triggers, iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi)
// {
//     trigger_hlt_matched_output trig_out {false, 0};
//     for (auto &trigger: triggers) {
//         if (!trigger.pass)
//             continue;

//         int passMandObjs = 0;
//         int totObjs = int( trigger.id.size() );
//         int totMandObjs = (int) mandatory_objs.size();
//         std::vector<bool> trigObjPassed;
//         trigObjPassed.resize(totObjs);

//         for (int idx = 0; idx < totMandObjs; ++idx) {
//           for (int jdx = 0; jdx < totObjs; jdx++) {
//             if (trigObjPassed[jdx])
//                 continue;

//             if (applyOfln &&
//                  (mandatory_objs.at(idx).Pt() < trigger.pt.at(jdx) || 
//                   abs(mandatory_objs.at(idx).Eta()) > trigger.eta.at(jdx)))
//                 continue;

//             std::vector<int> all_bits;
//             all_bits.insert(all_bits.end(), trigger.bits.at(jdx).begin(), trigger.bits.at(jdx).end());
//             if (trigger.additional_bits_mand_objs.size())
//               all_bits.insert(all_bits.end(), trigger.additional_bits_mand_objs.at(jdx).begin(), trigger.additional_bits_mand_objs.at(jdx).end());
//             if (!match_trigger_object(mandatory_objs.at(idx).Eta(), mandatory_objs.at(idx).Phi(), trigger.id.at(jdx),
//                 TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, all_bits))
//                 continue;

//             trigObjPassed[jdx] = true;
//             trig_out.HLTMatchedJets += (1 << idx);
//             passMandObjs += 1;
//             break;
//           }
//         }

//         if (passMandObjs != totMandObjs)
//           continue;

//         int passOptObjs = 0;
//         int totOptObjs = (int) optional_objs.size();

//         for (int idx = 0; idx < totOptObjs; ++idx) {
//           for (int jdx = 0; jdx < totObjs; jdx++) {
//             if (trigObjPassed[jdx])
//                 continue;

//             if (applyOfln &&
//                  (optional_objs.at(idx).Pt() < trigger.pt.at(jdx) || 
//                   abs(optional_objs.at(idx).Eta()) > trigger.eta.at(jdx)))
//                 continue;

//             if (!match_trigger_object(optional_objs.at(idx).Eta(), optional_objs.at(idx).Phi(), trigger.id.at(jdx),
//                 TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, trigger.bits.at(jdx)))
//                 continue;

//             trigObjPassed[jdx] = true;
//             trig_out.HLTMatchedJets += (1 << (totMandObjs + idx));
//             passOptObjs += 1;
//             break;
//           }
//         }

//         if (passMandObjs + passOptObjs == totObjs) {
//           trig_out.trigger_passed = true;
//           return trig_out;
//         }
//     }
//     return trig_out;
// }


//------------------------------------------------------------------------
// stuctures and functions needed mainly for HHLeptonInterface

// pairSorting for MVA isolations: higher score --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortMVA (const tau_pair& pA, const tau_pair& pB)
{
  float isoA = pA.iso1;
  float isoB = pB.iso1;
  if (isoA > isoB) return true;
  else if (isoA < isoB) return false;

  float ptA = pA.pt1;
  float ptB = pB.pt1;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  isoA = pA.iso2;
  isoB = pB.iso2;
  if (isoA > isoB) return true;
  else if (isoA < isoB) return false;

  ptA = pA.pt2;
  ptB = pB.pt2;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  // should be never here..
  return false;
}

// pairSorting for Raw isolations: lower iso value --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortRawIso (const tau_pair& pA, const tau_pair& pB)
{
  float isoA = pA.iso1;
  float isoB = pB.iso1;
  if (isoA < isoB) return true;
  else if (isoA > isoB) return false;

  float ptA = pA.pt1;
  float ptB = pB.pt1;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  isoA = pA.iso2;
  isoB = pB.iso2;
  if (isoA < isoB) return true;
  else if (isoA > isoB) return false;

  ptA = pA.pt2;
  ptB = pB.pt2;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  // should be never here..
  return false;
}

// pairSorting for Lep+Tauh:
//  - leg1 (lepton): lower iso value --> more isolated
//  - leg2 (tauh)  : higher score    --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortHybrid (const tau_pair& pA, const tau_pair& pB)
{
  float isoA = pA.iso1;
  float isoB = pB.iso1;
  if (isoA < isoB) return true;
  else if (isoA > isoB) return false;

  float ptA = pA.pt1;
  float ptB = pB.pt1;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  isoA = pA.iso2;
  isoB = pB.iso2;
  if (isoA > isoB) return true;
  else if (isoA < isoB) return false;

  ptA = pA.pt2;
  ptB = pB.pt2;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  // should be never here..
  return false;
}

//------------------------------------------------------------------------
// functions needed mainly for HHJetsInterface
bool jetSort (const jet_idx_btag& jA, const jet_idx_btag& jB)
{
  return (jA.btag > jB.btag);
}

bool jetPairSort (const jet_pair_mass& jA, const jet_pair_mass& jB)
{
  return (jA.inv_mass > jB.inv_mass);
}

bool jetPtSort (const TLorentzVector& jA, const TLorentzVector& jB)
{
  return (jA.Pt() > jB.Pt());
}

leps GetLeps(
  fRVec muonpt, fRVec muoneta, fRVec muonphi, fRVec muonmass, iRVec muoncharge,
  fRVec electronpt, fRVec electroneta, fRVec electronphi, fRVec electronmass, iRVec electroncharge,
  fRVec taupt, fRVec taueta, fRVec tauphi, fRVec taumass, iRVec taucharge, iRVec taudecaymode,
  fRVec tadeeptauvse, fRVec taudeeptauvsmu, fRVec taudeeptauvsjet,
  int dau1_index, int dau2_index, int pairType)
{
  leps leptonsCollection;
  if (pairType == 0) { // MuTau
      leptonsCollection.dau1_pt     = muonpt.at(dau1_index);
      leptonsCollection.dau1_eta    = muoneta.at(dau1_index);
      leptonsCollection.dau1_phi    = muonphi.at(dau1_index);
      leptonsCollection.dau1_mass   = muonmass.at(dau1_index);
      leptonsCollection.dau1_charge = muoncharge.at(dau1_index);
      
      leptonsCollection.dau2_pt     = taupt.at(dau2_index);
      leptonsCollection.dau2_eta    = taueta.at(dau2_index);
      leptonsCollection.dau2_phi    = tauphi.at(dau2_index);
      leptonsCollection.dau2_mass   = taumass.at(dau2_index);
      leptonsCollection.dau2_charge = taucharge.at(dau2_index);
      leptonsCollection.dau2_DM     = taudecaymode.at(dau2_index);
      leptonsCollection.dau2_tauIdVSe   = tadeeptauvse.at(dau2_index);
      leptonsCollection.dau2_tauIdVSmu  = taudeeptauvsmu.at(dau2_index);
      leptonsCollection.dau2_tauIdVSjet = taudeeptauvsjet.at(dau2_index);
  } 

  else if (pairType == 1) { // ETau
      leptonsCollection.dau1_pt     = electronpt.at(dau1_index);
      leptonsCollection.dau1_eta    = electroneta.at(dau1_index);
      leptonsCollection.dau1_phi    = electronphi.at(dau1_index);
      leptonsCollection.dau1_mass   = electronmass.at(dau1_index);
      leptonsCollection.dau1_charge = electroncharge.at(dau1_index);
      
      leptonsCollection.dau2_pt     = taupt.at(dau2_index);
      leptonsCollection.dau2_eta    = taueta.at(dau2_index);
      leptonsCollection.dau2_phi    = tauphi.at(dau2_index);
      leptonsCollection.dau2_mass   = taumass.at(dau2_index);
      leptonsCollection.dau2_charge = taucharge.at(dau2_index);
      leptonsCollection.dau2_DM     = taudecaymode.at(dau2_index);
      leptonsCollection.dau2_tauIdVSe   = tadeeptauvse.at(dau2_index);
      leptonsCollection.dau2_tauIdVSmu  = taudeeptauvsmu.at(dau2_index);
      leptonsCollection.dau2_tauIdVSjet = taudeeptauvsjet.at(dau2_index);
  } 

  else if (pairType == 2) { // TauTau
      leptonsCollection.dau1_pt     = taupt.at(dau1_index);
      leptonsCollection.dau1_eta    = taueta.at(dau1_index);
      leptonsCollection.dau1_phi    = tauphi.at(dau1_index);
      leptonsCollection.dau1_mass   = taumass.at(dau1_index);
      leptonsCollection.dau1_charge = taucharge.at(dau1_index);
      leptonsCollection.dau1_DM     = taudecaymode.at(dau1_index);
      leptonsCollection.dau1_tauIdVSe   = tadeeptauvse.at(dau1_index);
      leptonsCollection.dau1_tauIdVSmu  = taudeeptauvsmu.at(dau1_index);
      leptonsCollection.dau1_tauIdVSjet = taudeeptauvsjet.at(dau1_index);
      
      leptonsCollection.dau2_pt     = taupt.at(dau2_index);
      leptonsCollection.dau2_eta    = taueta.at(dau2_index);
      leptonsCollection.dau2_phi    = tauphi.at(dau2_index);
      leptonsCollection.dau2_mass   = taumass.at(dau2_index);
      leptonsCollection.dau2_charge = taucharge.at(dau2_index);
      leptonsCollection.dau2_DM     = taudecaymode.at(dau2_index);
      leptonsCollection.dau2_tauIdVSe   = tadeeptauvse.at(dau2_index);
      leptonsCollection.dau2_tauIdVSmu  = taudeeptauvsmu.at(dau2_index);
      leptonsCollection.dau2_tauIdVSjet = taudeeptauvsjet.at(dau2_index);
  } 

  else if (pairType == 3) { // MuMu
      leptonsCollection.dau1_pt     = muonpt.at(dau1_index);
      leptonsCollection.dau1_eta    = muoneta.at(dau1_index);
      leptonsCollection.dau1_phi    = muonphi.at(dau1_index);
      leptonsCollection.dau1_mass   = muonmass.at(dau1_index);
      leptonsCollection.dau1_charge = muoncharge.at(dau1_index);
      
      leptonsCollection.dau2_pt     = muonpt.at(dau2_index);
      leptonsCollection.dau2_eta    = muoneta.at(dau2_index);
      leptonsCollection.dau2_phi    = muonphi.at(dau2_index);
      leptonsCollection.dau2_mass   = muonmass.at(dau2_index);
      leptonsCollection.dau2_charge = muoncharge.at(dau2_index);
  } 

  else if (pairType == 4) { // EE
      leptonsCollection.dau1_pt     = electronpt.at(dau1_index);
      leptonsCollection.dau1_eta    = electroneta.at(dau1_index);
      leptonsCollection.dau1_phi    = electronphi.at(dau1_index);
      leptonsCollection.dau1_mass   = electronmass.at(dau1_index);
      leptonsCollection.dau1_charge = electroncharge.at(dau1_index);
      
      leptonsCollection.dau2_pt     = electronpt.at(dau2_index);
      leptonsCollection.dau2_eta    = electroneta.at(dau2_index);
      leptonsCollection.dau2_phi    = electronphi.at(dau2_index);
      leptonsCollection.dau2_mass   = electronmass.at(dau2_index);
      leptonsCollection.dau2_charge = electroncharge.at(dau2_index);
  } 

  else if (pairType == 5) { // EMu
      leptonsCollection.dau1_pt     = muonpt.at(dau1_index);
      leptonsCollection.dau1_eta    = muoneta.at(dau1_index);
      leptonsCollection.dau1_phi    = muonphi.at(dau1_index);
      leptonsCollection.dau1_mass   = muonmass.at(dau1_index);
      leptonsCollection.dau1_charge = muoncharge.at(dau1_index);
      
      leptonsCollection.dau2_pt     = electronpt.at(dau2_index);
      leptonsCollection.dau2_eta    = electroneta.at(dau2_index);
      leptonsCollection.dau2_phi    = electronphi.at(dau2_index);
      leptonsCollection.dau2_mass   = electronmass.at(dau2_index);
      leptonsCollection.dau2_charge = electroncharge.at(dau2_index);
  }

  return leptonsCollection;
}

std::pair<TLorentzVector, TLorentzVector> GetLepsTLV(
    int index1, int index2,
    fRVec lep1_pt, fRVec lep1_eta, fRVec lep1_phi, fRVec lep1_mass,
    fRVec lep2_pt, fRVec lep2_eta, fRVec lep2_phi, fRVec lep2_mass)
{
    auto lep1_tlv = TLorentzVector(), lep2_tlv = TLorentzVector();
    lep1_tlv.SetPtEtaPhiM(lep1_pt[index1], lep1_eta[index1], lep1_phi[index1], lep1_mass[index1]);
    lep2_tlv.SetPtEtaPhiM(lep2_pt[index2], lep2_eta[index2], lep2_phi[index2], lep2_mass[index2]);
    return std::make_pair<TLorentzVector, TLorentzVector>((TLorentzVector) lep1_tlv, (TLorentzVector) lep2_tlv);
}

jets GetJets (
    bool hasresolvedak4, int bjet1_jetidx, int bjet2_jetidx,
    bool hasvbfak4, int vbfjet1_jetidx, int vbfjet2_jetidx,
    bool hasboostedak8, int fatjet_jetidx,
    fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass, fRVec jet_btag,
    fRVec fatjet_pt, fRVec fatjet_eta, fRVec fatjet_phi, fRVec fatjet_mass, fRVec fatjet_btag)
{
    jets jetsCollection;
    if (hasresolvedak4) {
        jetsCollection.bjet1_pt = jet_pt.at(bjet1_jetidx);
        jetsCollection.bjet1_eta = jet_eta.at(bjet1_jetidx);
        jetsCollection.bjet1_phi = jet_phi.at(bjet1_jetidx);
        jetsCollection.bjet1_mass = jet_mass.at(bjet1_jetidx);
        jetsCollection.bjet1_btag = jet_btag.at(bjet1_jetidx);

        jetsCollection.bjet2_pt = jet_pt.at(bjet2_jetidx);
        jetsCollection.bjet2_eta = jet_eta.at(bjet2_jetidx);
        jetsCollection.bjet2_phi = jet_phi.at(bjet2_jetidx);
        jetsCollection.bjet2_mass = jet_mass.at(bjet2_jetidx);
        jetsCollection.bjet2_btag = jet_btag.at(bjet2_jetidx);
    }

    if (hasboostedak8) {
        jetsCollection.fatbjet_pt = fatjet_pt.at(fatjet_jetidx);
        jetsCollection.fatbjet_eta = fatjet_eta.at(fatjet_jetidx);
        jetsCollection.fatbjet_phi = fatjet_phi.at(fatjet_jetidx);
        jetsCollection.fatbjet_mass = fatjet_mass.at(fatjet_jetidx);
        jetsCollection.fatbjet_btag = fatjet_btag.at(fatjet_jetidx);
    }

    if (hasvbfak4) {
        jetsCollection.vbfjet1_pt = jet_pt.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_eta = jet_eta.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_phi = jet_phi.at(vbfjet1_jetidx);
        jetsCollection.vbfjet1_mass = jet_mass.at(vbfjet1_jetidx);

        jetsCollection.vbfjet2_pt = jet_pt.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_eta = jet_eta.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_phi = jet_phi.at(vbfjet2_jetidx);
        jetsCollection.vbfjet2_mass = jet_mass.at(vbfjet2_jetidx);
    }

    return jetsCollection;
}

std::pair<TLorentzVector, TLorentzVector> GetJetsTLV(
    int index1, int index2,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass)
{
    auto bjet1_tlv = TLorentzVector(), bjet2_tlv = TLorentzVector();
    if (index1 >= 0) {
        bjet1_tlv.SetPtEtaPhiM(Jet_pt[index1], Jet_eta[index1], Jet_phi[index1], Jet_mass[index1]);
    } 
    else {
        bjet1_tlv.SetPtEtaPhiM(-1, 0, 0, 0);
    }
    if (index2 >= 0) {
        bjet2_tlv.SetPtEtaPhiM(Jet_pt[index2], Jet_eta[index2], Jet_phi[index2], Jet_mass[index2]);
    } 
    else {
        bjet2_tlv.SetPtEtaPhiM(-1, 0, 0, 0);
    }
    return std::make_pair<TLorentzVector, TLorentzVector>((TLorentzVector) bjet1_tlv, (TLorentzVector) bjet2_tlv);
}
